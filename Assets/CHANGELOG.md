# Changelog

## [0.0.7] 01-09-2019
### Removed
	-Remove #if in modules

## [0.0.6] 01-09-2019
### Change
	-Change all internal to public
	-Refactor name class

## [0.0.5] - 31-08-2019
### Changed
	-Update dotween to v1.2.270

## [0.0.4] - 12-08-2019

* Add Module TextmeshPro , remove resource folder

## [0.0.3] - 12-08-2019

* Remove unuse editor code to support upm

## [0.0.1] - 04-08-2019

* Initial version