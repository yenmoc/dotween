﻿// Author: Daniele Giardini - http://www.demigiant.com
// Created: 2014/12/24 13:50

using System;
using System.IO;
using System.Reflection;
using System.Text;
using DG.Tweening;
using UnityEditor;
using UnityEngine;

namespace DG.DOTweenEditor
{
    public static class EditorUtils
    {
        public static string ProjectPath { get; private set; } // Without final slash
        public static string AssetsPath { get; private set; } // Without final slash
        // Editor path from Assets (not included) with final slash, in AssetDatabase format (/)
        public static string EditorAdbDir { get { if (string.IsNullOrEmpty(editorAdbDir)) StoreEditorAdbDir(); return editorAdbDir; } }
        // With final slash (system based) - might be NULL in case users are not using a parent Demigiant folder
        // With final slash (system based)
        public static string DotweenDir { get { if (string.IsNullOrEmpty(dotweenDir)) StoreDoTweenDirs(); return dotweenDir; } }
        // With final slash (system based)
        public static bool IsOsxEditor { get; private set; }
        public static string PathSlash { get; private set; } // for full paths
        public static string PathSlashToReplace { get; private set; } // for full paths

        private static readonly StringBuilder Strb = new StringBuilder();
        private static string editorAdbDir;
        private static string dotweenDir; // with final slash// with final slash

        static EditorUtils()
        {
            IsOsxEditor = Application.platform == RuntimePlatform.OSXEditor;
            var useWindowsSlashes = Application.platform == RuntimePlatform.WindowsEditor;
            PathSlash = useWindowsSlashes ? "\\" : "/";
            PathSlashToReplace = useWindowsSlashes ? "/" : "\\";

            ProjectPath = Application.dataPath;
            ProjectPath = ProjectPath.Substring(0, ProjectPath.LastIndexOf("/"));
            ProjectPath = ProjectPath.Replace(PathSlashToReplace, PathSlash);
            
            AssetsPath = ProjectPath + PathSlash + "Assets";
        }

        // ===================================================================================
        // PUBLIC METHODS --------------------------------------------------------------------

        private static void DeleteAssetsIfExist(string[] adbFilePaths)
        {
            foreach (var f in adbFilePaths) {
                if (AssetExists(f)) AssetDatabase.DeleteAsset(f);
            }
        }
//        static void ReimportAssets(string[] adbFilePaths)
//        {
//            foreach (string f in adbFilePaths) {
//                if (AssetExists(f)) AssetDatabase.ImportAsset(f, ImportAssetOptions.ForceUpdate);
//            }
//        }

        /// <summary>
        /// Returns TRUE if the file/directory at the given path exists.
        /// </summary>
        /// <param name="adbPath">Path, relative to Unity's project folder</param>
        /// <returns></returns>
        public static bool AssetExists(string adbPath)
        {
            var fullPath = AdbPathToFullPath(adbPath);
            return File.Exists(fullPath) || Directory.Exists(fullPath);
        }

        /// <summary>
        /// Converts the given project-relative path to a full path,
        /// with backward (\) slashes).
        /// </summary>
        public static string AdbPathToFullPath(string adbPath)
        {
            adbPath = adbPath.Replace(PathSlashToReplace, PathSlash);
            return ProjectPath + PathSlash + adbPath;
        }

        /// <summary>
        /// Converts the given full path to a path usable with AssetDatabase methods
        /// (relative to Unity's project folder, and with the correct Unity forward (/) slashes).
        /// </summary>
        public static string FullPathToAdbPath(string fullPath)
        {
            var adbPath = fullPath.Substring(ProjectPath.Length + 1);
            return adbPath.Replace("\\", "/");
        }

        /// <summary>
        /// Connects to a <see cref="ScriptableObject"/> asset.
        /// If the asset already exists at the given path, loads it and returns it.
        /// Otherwise, either returns NULL or automatically creates it before loading and returning it
        /// (depending on the given parameters).
        /// </summary>
        /// <typeparam name="T">Asset type</typeparam>
        /// <param name="adbFilePath">File path (relative to Unity's project folder)</param>
        /// <param name="createIfMissing">If TRUE and the requested asset doesn't exist, forces its creation</param>
        public static T ConnectToSourceAsset<T>(string adbFilePath, bool createIfMissing = false) where T : ScriptableObject
        {
            if (!AssetExists(adbFilePath)) {
                if (createIfMissing) CreateScriptableAsset<T>(adbFilePath);
                else return null;
            }
            var source = (T)AssetDatabase.LoadAssetAtPath(adbFilePath, typeof(T));
            if (source == null) {
                // Source changed (or editor file was moved from outside of Unity): overwrite it
                CreateScriptableAsset<T>(adbFilePath);
                source = (T)AssetDatabase.LoadAssetAtPath(adbFilePath, typeof(T));
            }
            return source;
        }

        /// <summary>
        /// Full path for the given loaded assembly, assembly file included
        /// </summary>
        public static string GetAssemblyFilePath(Assembly assembly)
        {

            var codeBase = assembly.CodeBase;
            var uri = new UriBuilder(codeBase);
            var path = Uri.UnescapeDataString(uri.Path);
            if (path.Substring(path.Length - 3) == "dll") return path;

//            string codeBase = assembly.CodeBase;
//            UriBuilder uri = new UriBuilder(codeBase);
//            string path = Path.GetDirectoryName(Uri.UnescapeDataString(uri.Path));
//            string lastChar = path.Substring(path.Length - 4);
//            if (lastChar == "dll") return path;

            // Invalid path, use Location
            return Path.GetFullPath(assembly.Location);
        }

        /// <summary>
        /// Adds the given global define if it's not already present
        /// </summary>

        // ===================================================================================
        // METHODS ---------------------------------------------------------------------------

        // AssetDatabase formatted path to DOTween's Editor folder
        private static void StoreEditorAdbDir()
        {
//            string codeBase = Assembly.GetExecutingAssembly().CodeBase;
//            UriBuilder uri = new UriBuilder(codeBase);
//            string fullPath = Path.GetDirectoryName(Uri.UnescapeDataString(uri.Path));
            var fullPath = Path.GetDirectoryName(GetAssemblyFilePath(Assembly.GetExecutingAssembly()));
            var adbPath = fullPath.Substring(Application.dataPath.Length + 1);
            editorAdbDir = adbPath.Replace("\\", "/") + "/";
        }

        private static void StoreDoTweenDirs()
        {
//            string codeBase = Assembly.GetExecutingAssembly().CodeBase;
//            UriBuilder uri = new UriBuilder(codeBase);
//            _dotweenDir = Path.GetDirectoryName(Uri.UnescapeDataString(uri.Path));
            dotweenDir = AssetsPath + PathSlash;
            var pathSeparator = dotweenDir.IndexOf("/") != -1 ? "/" : "\\";
            dotweenDir = dotweenDir.Substring(0, dotweenDir.LastIndexOf(pathSeparator) + 1);

            dotweenDir = dotweenDir.Replace(PathSlashToReplace, PathSlash);
            dotweenDir = dotweenDir.Replace(PathSlashToReplace, PathSlash);
        }

        private static void CreateScriptableAsset<T>(string adbFilePath) where T : ScriptableObject
        {
            if (!Directory.Exists(adbFilePath))
            {
                Directory.CreateDirectory(adbFilePath);
            }
            var data = ScriptableObject.CreateInstance<T>();
            AssetDatabase.CreateAsset(data, adbFilePath);
        }

        private static bool IsValidBuildTargetGroup(BuildTargetGroup group)
        {
            if (group == BuildTargetGroup.Unknown) return false;
            var moduleManager = Type.GetType("UnityEditor.Modules.ModuleManager, UnityEditor.dll");
//            MethodInfo miIsPlatformSupportLoaded = moduleManager.GetMethod("IsPlatformSupportLoaded", BindingFlags.Static | BindingFlags.NonPublic);
            var miGetTargetStringFromBuildTargetGroup = moduleManager.GetMethod(
                "GetTargetStringFromBuildTargetGroup", BindingFlags.Static | BindingFlags.NonPublic
            );
            var miGetPlatformName = typeof(PlayerSettings).GetMethod(
                "GetPlatformName", BindingFlags.Static | BindingFlags.NonPublic
            );
            var targetString = (string)miGetTargetStringFromBuildTargetGroup.Invoke(null, new object[] {group});
            var platformName = (string)miGetPlatformName.Invoke(null, new object[] {group});

            // Group is valid if at least one betweeen targetString and platformName is not empty.
            // This seems to me the safest and more reliant way to check,
            // since ModuleManager.IsPlatformSupportLoaded dosn't work well with BuildTargetGroup (only BuildTarget)
            var isValid = !string.IsNullOrEmpty(targetString) || !string.IsNullOrEmpty(platformName);

//            Debug.Log((isValid ? "<color=#00ff00>" : "<color=#ff0000>") + group + " > " + targetString + " / " + platformName + " > "  + isValid + "/" + miIsPlatformSupportLoaded.Invoke(null, new object[] {group.ToString()}) + "</color>");
            return isValid;
        }
    }
}