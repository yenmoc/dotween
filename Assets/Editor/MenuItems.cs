﻿// Author: Daniele Giardini - http://www.demigiant.com
// Created: 2018/08/07 18:05
// License Copyright (c) Daniele Giardini
// This work is subject to the terms at http://dotween.demigiant.com/license.php

using DG.Tweening.Core;
using UnityEditor;
using UnityEngine;

// ReSharper disable once CheckNamespace
namespace DG.DOTweenEditor
{
    public static class MenuItems
    {
        [MenuItem("Tools/Demigiant/DOTween Manager", false, 20)]
        private static void CreateDoTweenComponent(MenuCommand menuCommand)
        {
            var go = new GameObject("[DOTween]");
            go.AddComponent<DoTweenComponent>();
            GameObjectUtility.SetParentAndAlign(go, menuCommand.context as GameObject);
            Undo.RegisterCreatedObjectUndo(go, "Create " + go.name);
            Selection.activeObject = go;
        }
    }
}