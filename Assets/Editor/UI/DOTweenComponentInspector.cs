﻿// Author: Daniele Giardini - http://www.demigiant.com
// Created: 2014/06/29 20:37
// 
// License Copyright (c) Daniele Giardini.
// This work is subject to the terms at http://dotween.demigiant.com/license.php

using System.Text;
using DG.Tweening;
using DG.Tweening.Core;
using UnityEditor;
using UnityEngine;

// ReSharper disable once CheckNamespace
namespace DG.DOTweenEditor.UI
{
    [CustomEditor(typeof(DoTweenComponent))]
    public class DoTweenComponentInspector : Editor
    {
        private DoTweenSettings _settings;
        private string _title;
        private readonly StringBuilder _strb = new StringBuilder();
        private bool _isRuntime;
        private string _playingTweensHex;
        private string _pausedTweensHex;

        #region Unity + GUI

        private void OnEnable()
        {
            _isRuntime = EditorApplication.isPlaying;
            ConnectToSource(true);

            _strb.Length = 0;
            _strb.Append("DOTween v").Append(DoTween.Version);
            if (TweenManager.isDebugBuild) _strb.Append(" [Debug build]");
            else _strb.Append(" [Release build]");

            _title = _strb.ToString();

            _playingTweensHex = EditorGUIUtility.isProSkin ? "<color=#00c514>" : "<color=#005408>";
            _pausedTweensHex = EditorGUIUtility.isProSkin ? "<color=#ff832a>" : "<color=#873600>";
        }

        public override void OnInspectorGUI()
        {
            _isRuntime = EditorApplication.isPlaying;
            ConnectToSource();

            EditorGuiUtils.SetGuiStyles();

            GUILayout.Space(4);
            GUILayout.BeginHorizontal();
            GUILayout.Label(_isRuntime ? "RUNTIME MODE" : "EDITOR MODE");
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();

            var totActiveTweens = TweenManager.totActiveTweens;
            var totPlayingTweens = TweenManager.TotalPlayingTweens();
            var totPausedTweens = totActiveTweens - totPlayingTweens;
            var totActiveDefaultTweens = TweenManager.totActiveDefaultTweens;
            var totActiveLateTweens = TweenManager.totActiveLateTweens;
            var totActiveFixedTweens = TweenManager.totActiveFixedTweens;
            var totActiveManualTweens = TweenManager.totActiveManualTweens;

            GUILayout.Label(_title, TweenManager.isDebugBuild ? EditorGuiUtils.redLabelStyle : EditorGuiUtils.boldLabelStyle);

            if (!_isRuntime)
            {
                GUI.backgroundColor = new Color(0f, 0.31f, 0.48f);
                GUI.contentColor = Color.white;
                GUILayout.Label(
                    "This component is <b>added automatically</b> by DOTween at runtime." +
                    "\nAdding it yourself is <b>not recommended</b> unless you really know what you're doing:" +
                    " you'll have to be sure it's <b>never destroyed</b> and that it's present <b>in every scene</b>.",
                    EditorGuiUtils.infoboxStyle
                );
                GUI.backgroundColor = GUI.contentColor = GUI.contentColor = Color.white;
            }

            GUILayout.Space(6);
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Documentation")) Application.OpenURL("http://dotween.demigiant.com/documentation.php");
            if (GUILayout.Button("Check Updates")) Application.OpenURL("http://dotween.demigiant.com/download.php?v=" + DoTween.Version);
            GUILayout.EndHorizontal();

            if (_isRuntime)
            {
                GUILayout.BeginHorizontal();
                if (GUILayout.Button(_settings.showPlayingTweens ? "Hide Playing Tweens" : "Show Playing Tweens"))
                {
                    _settings.showPlayingTweens = !_settings.showPlayingTweens;
                    EditorUtility.SetDirty(_settings);
                }

                if (GUILayout.Button(_settings.showPausedTweens ? "Hide Paused Tweens" : "Show Paused Tweens"))
                {
                    _settings.showPausedTweens = !_settings.showPausedTweens;
                    EditorUtility.SetDirty(_settings);
                }

                GUILayout.EndHorizontal();
                GUILayout.BeginHorizontal();
                if (GUILayout.Button("Play all")) DoTween.PlayAll();
                if (GUILayout.Button("Pause all")) DoTween.PauseAll();
                if (GUILayout.Button("Kill all")) DoTween.KillAll();
                GUILayout.EndHorizontal();

                GUILayout.Space(8);
                GUILayout.Label("<b>Legend: </b> TW = Tweener, SE = Sequence", EditorGuiUtils.wordWrapRichTextLabelStyle);

                GUILayout.Space(8);
                _strb.Length = 0;
                _strb.Append("Active tweens: ").Append(totActiveTweens)
                    .Append(" (").Append(TweenManager.totActiveTweeners).Append(" TW, ")
                    .Append(TweenManager.totActiveSequences).Append(" SE)")
                    .Append("\nDefault/Late/Fixed/Manual tweens: ").Append(totActiveDefaultTweens)
                    .Append("/").Append(totActiveLateTweens)
                    .Append("/").Append(totActiveFixedTweens)
                    .Append("/").Append(totActiveManualTweens)
                    .Append(_playingTweensHex).Append("\nPlaying tweens: ").Append(totPlayingTweens);
                if (_settings.showPlayingTweens)
                {
                    foreach (var t in TweenManager.activeTweens)
                    {
                        if (t == null || !t.isPlaying) continue;
                        _strb.Append("\n   - [").Append(t.tweenType == TweenType.Tweener ? "TW" : "SE");
                        AppendTweenIdLabel(_strb, t);
                        _strb.Append("] ").Append(GetTargetTypeLabel(t.target));
                    }
                }

                _strb.Append("</color>");
                _strb.Append(_pausedTweensHex).Append("\nPaused tweens: ").Append(totPausedTweens);
                if (_settings.showPausedTweens)
                {
                    foreach (var t in TweenManager.activeTweens)
                    {
                        if (t == null || t.isPlaying) continue;
                        _strb.Append("\n   - [").Append(t.tweenType == TweenType.Tweener ? "TW" : "SE");
                        AppendTweenIdLabel(_strb, t);
                        _strb.Append("] ").Append(GetTargetTypeLabel(t.target));
                    }
                }

                _strb.Append("</color>");
                _strb.Append("\nPooled tweens: ").Append(TweenManager.TotalPooledTweens())
                    .Append(" (").Append(TweenManager.totPooledTweeners).Append(" TW, ")
                    .Append(TweenManager.totPooledSequences).Append(" SE)");
                GUILayout.Label(_strb.ToString(), EditorGuiUtils.wordWrapRichTextLabelStyle);

                GUILayout.Space(8);
                _strb.Remove(0, _strb.Length);
                _strb.Append("Tweens Capacity: ").Append(TweenManager.maxTweeners).Append(" TW, ").Append(TweenManager.maxSequences).Append(" SE")
                    .Append("\nMax Simultaneous Active Tweens: ").Append(DoTween.maxActiveTweenersReached).Append(" TW, ")
                    .Append(DoTween.maxActiveSequencesReached).Append(" SE");
                GUILayout.Label(_strb.ToString(), EditorGuiUtils.wordWrapRichTextLabelStyle);
            }

            GUILayout.Space(8);
            _strb.Remove(0, _strb.Length);
            _strb.Append("<b>SETTINGS ▼</b>");
            _strb.Append("\nSafe Mode: ").Append((_isRuntime ? DoTween.useSafeMode : _settings.useSafeMode) ? "ON" : "OFF");
            _strb.Append("\nLog Behaviour: ").Append(_isRuntime ? DoTween.LogBehaviour : _settings.logBehaviour);
            _strb.Append("\nShow Unity Editor Report: ").Append(_isRuntime ? DoTween.showUnityEditorReport : _settings.showUnityEditorReport);
            _strb.Append("\nTimeScale (Unity/DOTween): ").Append(Time.timeScale).Append("/").Append(_isRuntime ? DoTween.timeScale : _settings.timeScale);
            GUILayout.Label(_strb.ToString(), EditorGuiUtils.wordWrapRichTextLabelStyle);
            GUILayout.Label(
                "NOTE: DOTween's TimeScale is not the same as Unity's Time.timeScale: it is actually multiplied by it except for tweens that are set to update independently",
                EditorGuiUtils.wordWrapRichTextLabelStyle
            );

            GUILayout.Space(8);
            _strb.Remove(0, _strb.Length);
            _strb.Append("<b>DEFAULTS ▼</b>");
            _strb.Append("\ndefaultRecyclable: ").Append(_isRuntime ? DoTween.defaultRecyclable : _settings.defaultRecyclable);
            _strb.Append("\ndefaultUpdateType: ").Append(_isRuntime ? DoTween.defaultUpdateType : _settings.defaultUpdateType);
            _strb.Append("\ndefaultTSIndependent: ").Append(_isRuntime ? DoTween.defaultTimeScaleIndependent : _settings.defaultTimeScaleIndependent);
            _strb.Append("\ndefaultAutoKill: ").Append(_isRuntime ? DoTween.defaultAutoKill : _settings.defaultAutoKill);
            _strb.Append("\ndefaultAutoPlay: ").Append(_isRuntime ? DoTween.defaultAutoPlay : _settings.defaultAutoPlay);
            _strb.Append("\ndefaultEaseType: ").Append(_isRuntime ? DoTween.defaultEaseType : _settings.defaultEaseType);
            _strb.Append("\ndefaultLoopType: ").Append(_isRuntime ? DoTween.defaultLoopType : _settings.defaultLoopType);
            GUILayout.Label(_strb.ToString(), EditorGuiUtils.wordWrapRichTextLabelStyle);

            GUILayout.Space(10);
        }

        #endregion

        #region Methods

        private void ConnectToSource(bool forceReconnection = false)
        {
            if (_settings == null || forceReconnection)
            {
                _settings = _isRuntime
                    ? Resources.Load(DoTweenSettings.ASSET_NAME) as DoTweenSettings
                    : DoTweenUtilityWindow.GetDoTweenSettings();
            }
        }

        #endregion

        #region Helpers

        private void AppendTweenIdLabel(StringBuilder strb, Tween t)
        {
            if (!string.IsNullOrEmpty(t.stringId)) strb.Append(":<b>").Append(t.stringId).Append("</b>");
            else if (t.intId != -999) strb.Append(":<b>").Append(t.intId).Append("</b>");
            else if (t.id != null) strb.Append(":<b>").Append(t.id).Append("</b>");
        }

        private string GetTargetTypeLabel(object tweenTarget)
        {
            if (tweenTarget == null) return null;
            var s = tweenTarget.ToString();
            var dotIndex = s.LastIndexOf('.');
            if (dotIndex != -1) s = '(' + s.Substring(dotIndex + 1);
            return s;
        }

        #endregion
    }
}