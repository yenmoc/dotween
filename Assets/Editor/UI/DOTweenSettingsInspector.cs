﻿// Author: Daniele Giardini - http://www.demigiant.com
// Created: 2015/02/05 19:50

using DG.Tweening.Core;
using UnityEditor;

// ReSharper disable once CheckNamespace
namespace DG.DOTweenEditor.UI
{
    [CustomEditor(typeof(DoTweenSettings))]
    public class DoTweenSettingsInspector : Editor
    {
        private DoTweenSettings _src;

        // ===================================================================================
        // MONOBEHAVIOUR METHODS -------------------------------------------------------------

        private void OnEnable()
        {
            _src = target as DoTweenSettings;
        }

        public override void OnInspectorGUI()
        {
            UnityEngine.GUI.enabled = false;
            DrawDefaultInspector();
            UnityEngine.GUI.enabled = true;
        }
    }
}