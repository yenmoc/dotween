﻿// Author: Daniele Giardini - http://www.demigiant.com
// Created: 2014/12/24 13:37

using System.IO;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Core.Enums;
using UnityEditor;
using UnityEngine;

// ReSharper disable FieldCanBeMadeReadOnly.Local

// ReSharper disable once CheckNamespace
namespace DG.DOTweenEditor.UI
{
    public class DoTweenUtilityWindow : EditorWindow
    {
        [MenuItem("Tools/Demigiant/" + TITLE)]
        private static void ShowWindow()
        {
            Open();
        }

        private const string TITLE = "DOTween Utility Panel";
        private static readonly Vector2 WinSize = new Vector2(370, 510);
        public const string ID = "DOTweenVersion";

        private bool _initialized;
        private DoTweenSettings _src;
        private string _innerTitle;

        // If force is FALSE opens the window only if DOTween's version has changed
        // (set to FALSE by OnPostprocessAllAssets).<para/>
        // NOTE: this is also called via Reflection by UpgradeWindow
        public static void Open()
        {
            var window = GetWindow<DoTweenUtilityWindow>(true, TITLE, true);
            window.minSize = WinSize;
            window.maxSize = WinSize;
            window.ShowUtility();
            EditorPrefs.SetString(ID, DoTween.Version);
        }

        // ===================================================================================
        // UNITY METHODS ---------------------------------------------------------------------

        // Returns TRUE if DOTween is initialized
        private bool Init()
        {
            if (_initialized) return true;
            _initialized = true;
            return true;
        }


        private void OnHierarchyChange()
        {
            Repaint();
        }

        private void OnEnable()
        {
#if COMPATIBLE
            _innerTitle = "DOTween v" + DOTween.Version + " [Compatibility build]";
#else
            _innerTitle = "DOTween v" + DoTween.Version + (TweenManager.isDebugBuild ? " [Debug build]" : " [Release build]");
#endif
            Init();
        }

        private void OnGUI()
        {
            if (!Init())
            {
                GUILayout.Space(8);
                GUILayout.Label("Completing import process...");
                return;
            }

            Connect();
            EditorGuiUtils.SetGuiStyles(new Vector2(WinSize.x, 80));

            if (Application.isPlaying)
            {
                GUILayout.Space(40);
                GUILayout.BeginHorizontal();
                GUILayout.Space(40);
                GUILayout.Label("DOTween Utility Panel\nis disabled while in Play Mode", EditorGuiUtils.wrapCenterLabelStyle, GUILayout.ExpandWidth(true));
                GUILayout.Space(40);
                GUILayout.EndHorizontal();
            }
            else
            {
                var labelW = EditorGUIUtility.labelWidth;
                EditorGUIUtility.labelWidth = 200;
                EditorGUILayout.PrefixLabel(_innerTitle);
                DrawPreferencesGui();
                EditorGUIUtility.labelWidth = labelW;
            }

            if (GUI.changed) EditorUtility.SetDirty(_src);
        }

        // ===================================================================================
        // GUI METHODS -----------------------------------------------------------------------

        private void DrawPreferencesGui()
        {
            if (GUILayout.Button("Reset", EditorGuiUtils.btBigStyle))
            {
                // Reset to original defaults
                _src.useSafeMode = false;
                _src.safeModeOptions.nestedTweenFailureBehaviour = NestedTweenFailureBehaviour.TryToPreserveSequence;
                _src.showUnityEditorReport = false;
                _src.timeScale = 1;
                _src.useSmoothDeltaTime = false;
                _src.maxSmoothUnscaledTime = 0.15f;
                _src.rewindCallbackMode = RewindCallbackMode.FireIfPositionChanged;
                _src.logBehaviour = LogBehaviour.ErrorsOnly;
                _src.drawGizmos = true;
                _src.defaultRecyclable = false;
                _src.defaultAutoPlay = AutoPlay.All;
                _src.defaultUpdateType = UpdateType.Normal;
                _src.defaultTimeScaleIndependent = false;
                _src.defaultEaseType = Ease.OutQuad;
                _src.defaultEaseOvershootOrAmplitude = 1.70158f;
                _src.defaultEasePeriod = 0;
                _src.defaultAutoKill = true;
                _src.defaultLoopType = LoopType.Restart;
                EditorUtility.SetDirty(_src);
            }

            GUILayout.Space(8);
            _src.useSafeMode = EditorGUILayout.Toggle("Safe Mode", _src.useSafeMode);
            if (_src.useSafeMode)
            {
                _src.safeModeOptions.nestedTweenFailureBehaviour = (NestedTweenFailureBehaviour) EditorGUILayout.EnumPopup(
                    new GUIContent("└ On Nested Tween Failure", "Behaviour in case a tween inside a Sequence fails"),
                    _src.safeModeOptions.nestedTweenFailureBehaviour
                );
            }

            _src.timeScale = EditorGUILayout.FloatField("DOTween's TimeScale", _src.timeScale);
            _src.useSmoothDeltaTime = EditorGUILayout.Toggle("Smooth DeltaTime", _src.useSmoothDeltaTime);
            _src.maxSmoothUnscaledTime = EditorGUILayout.Slider("Max SmoothUnscaledTime", _src.maxSmoothUnscaledTime, 0.01f, 1f);
            _src.rewindCallbackMode = (RewindCallbackMode) EditorGUILayout.EnumPopup("OnRewind Callback Mode", _src.rewindCallbackMode);
            GUILayout.Space(-5);
            GUILayout.BeginHorizontal();
            GUILayout.Space(EditorGUIUtility.labelWidth + 4);
            EditorGUILayout.HelpBox(
                _src.rewindCallbackMode == RewindCallbackMode.FireIfPositionChanged
                    ? "When calling Rewind or PlayBackwards/SmoothRewind, OnRewind callbacks will be fired only if the tween isn't already rewinded"
                    : _src.rewindCallbackMode == RewindCallbackMode.FireAlwaysWithRewind
                        ? "When calling Rewind, OnRewind callbacks will always be fired, even if the tween is already rewinded."
                        : "When calling Rewind or PlayBackwards/SmoothRewind, OnRewind callbacks will always be fired, even if the tween is already rewinded",
                MessageType.None
            );
            GUILayout.EndHorizontal();
            _src.showUnityEditorReport = EditorGUILayout.Toggle("Editor Report", _src.showUnityEditorReport);
            _src.logBehaviour = (LogBehaviour) EditorGUILayout.EnumPopup("Log Behaviour", _src.logBehaviour);
            _src.drawGizmos = EditorGUILayout.Toggle("Draw Path Gizmos", _src.drawGizmos);

            Connect(true);

            GUILayout.Space(8);
            GUILayout.Label("DEFAULTS ▼");
            _src.defaultRecyclable = EditorGUILayout.Toggle("Recycle Tweens", _src.defaultRecyclable);
            _src.defaultAutoPlay = (AutoPlay) EditorGUILayout.EnumPopup("AutoPlay", _src.defaultAutoPlay);
            _src.defaultUpdateType = (UpdateType) EditorGUILayout.EnumPopup("Update Type", _src.defaultUpdateType);
            _src.defaultTimeScaleIndependent = EditorGUILayout.Toggle("TimeScale Independent", _src.defaultTimeScaleIndependent);
            _src.defaultEaseType = (Ease) EditorGUILayout.EnumPopup("Ease", _src.defaultEaseType);
            _src.defaultEaseOvershootOrAmplitude = EditorGUILayout.FloatField("Ease Overshoot", _src.defaultEaseOvershootOrAmplitude);
            _src.defaultEasePeriod = EditorGUILayout.FloatField("Ease Period", _src.defaultEasePeriod);
            _src.defaultAutoKill = EditorGUILayout.Toggle("AutoKill", _src.defaultAutoKill);
            _src.defaultLoopType = (LoopType) EditorGUILayout.EnumPopup("Loop Type", _src.defaultLoopType);
        }

        // ===================================================================================
        // METHODS ---------------------------------------------------------------------------

        public static DoTweenSettings GetDoTweenSettings()
        {
            return ConnectToSource(null, false, false);
        }

        private static DoTweenSettings ConnectToSource(DoTweenSettings src, bool createIfMissing, bool fullSetup)
        {
            var assetsLd = new LocationData(EditorUtils.AssetsPath + EditorUtils.PathSlash + "Resources");
            var dotweenLd = new LocationData(EditorUtils.DotweenDir + "Resources");

            if (src == null)
            {
                // Load eventual existing settings
                src = EditorUtils.ConnectToSourceAsset<DoTweenSettings>(assetsLd.adbFilePath, false);
                if (src == null) src = EditorUtils.ConnectToSourceAsset<DoTweenSettings>(dotweenLd.adbFilePath, false);
            }

            if (src == null)
            {
                // Settings don't exist.
                if (!createIfMissing) return null; // Stop here
                // Create it in external folder
                if (!Directory.Exists(assetsLd.dir)) AssetDatabase.CreateFolder(assetsLd.adbParentDir, "Resources");
                src = EditorUtils.ConnectToSourceAsset<DoTweenSettings>(assetsLd.adbFilePath, true);
            }

            return src;
        }

        private void Connect(bool forceReconnect = false)
        {
            if (_src != null && !forceReconnect) return;
            _src = ConnectToSource(_src, true, true);
        }

        // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        // ||| INTERNAL CLASSES ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

        private struct LocationData
        {
            public string dir; // without final slash
            public string filePath;
            public string adbFilePath;
            public string adbParentDir; // without final slash

            public LocationData(string srcDir) : this()
            {
                dir = srcDir;
                filePath = dir + EditorUtils.PathSlash + DoTweenSettings.ASSET_FULL_FILENAME;
                adbFilePath = EditorUtils.FullPathToAdbPath(filePath);
                adbParentDir = EditorUtils.FullPathToAdbPath(dir.Substring(0, dir.LastIndexOf(EditorUtils.PathSlash)));
            }
        }
    }
}