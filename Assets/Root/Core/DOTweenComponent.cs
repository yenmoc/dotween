﻿// Author: Daniele Giardini - http://www.demigiant.com
// Created: 2014/09/20 17:40
// 
// License Copyright (c) Daniele Giardini.
// This work is subject to the terms at http://dotween.demigiant.com/license.php

using System;
using System.Collections;
using System.Reflection;
using UnityEngine;

#pragma warning disable 1591
// ReSharper disable once CheckNamespace
namespace DG.Tweening.Core
{
    /// <summary>
    /// Used to separate DOTween class from the MonoBehaviour instance (in order to use static constructors on DOTween).
    /// Contains all instance-based methods
    /// </summary>
    [AddComponentMenu("")]
    public class DoTweenComponent : MonoBehaviour, IDoTweenInit
    {
        /// <summary>Used publicly inside Unity Editor, as a trick to update DOTween's inspector at every frame</summary>
        public int inspectorUpdater;

        private float _unscaledTime;
        private float _unscaledDeltaTime;

        private float _pausedTime; // Marks the time when Unity was paused

        private bool _duplicateToDestroy;

        #region Unity Methods

        private void Awake()
        {
            if (DoTween.instance == null) DoTween.instance = this;
            else
            {
                if (Debugger.LogPriority >= 1)
                {
                    Debugger.LogWarning("Duplicate DOTweenComponent instance found in scene: destroying it");
                }

                Destroy(this.gameObject);
                return;
            }

            inspectorUpdater = 0;
            _unscaledTime = Time.realtimeSinceStartup;

            // Initialize DOTweenModuleUtils via Reflection
            var modules = Utils.GetLooseScriptType("DG.Tweening.DoTweenModuleUtils");
            if (modules == null)
            {
                Debugger.LogError("Couldn't load Modules system");
                return;
            }

            var mi = modules.GetMethod("Init", BindingFlags.Static | BindingFlags.Public);
            mi.Invoke(null, null);
        }

        private void Start()
        {
            // Check if there's a leftover persistent DOTween object
            // (should be impossible but some weird Unity freeze caused that to happen on Seith's project
            if (DoTween.instance != this)
            {
                _duplicateToDestroy = true;
                Destroy(this.gameObject);
            }
        }

        private void Update()
        {
            _unscaledDeltaTime = Time.realtimeSinceStartup - _unscaledTime;
            if (DoTween.useSmoothDeltaTime && _unscaledDeltaTime > DoTween.maxSmoothUnscaledTime) _unscaledDeltaTime = DoTween.maxSmoothUnscaledTime;
            if (TweenManager.hasActiveDefaultTweens)
            {
                TweenManager.Update(UpdateType.Normal, (DoTween.useSmoothDeltaTime ? Time.smoothDeltaTime : Time.deltaTime) * DoTween.timeScale, _unscaledDeltaTime * DoTween.timeScale);
            }

            _unscaledTime = Time.realtimeSinceStartup;

            if (TweenManager.isUnityEditor)
            {
                inspectorUpdater++;
                if (DoTween.showUnityEditorReport && TweenManager.hasActiveTweens)
                {
                    if (TweenManager.totActiveTweeners > DoTween.maxActiveTweenersReached) DoTween.maxActiveTweenersReached = TweenManager.totActiveTweeners;
                    if (TweenManager.totActiveSequences > DoTween.maxActiveSequencesReached) DoTween.maxActiveSequencesReached = TweenManager.totActiveSequences;
                }
            }
        }

        private void LateUpdate()
        {
            if (TweenManager.hasActiveLateTweens)
            {
                TweenManager.Update(UpdateType.Late, (DoTween.useSmoothDeltaTime ? Time.smoothDeltaTime : Time.deltaTime) * DoTween.timeScale, _unscaledDeltaTime * DoTween.timeScale);
            }
        }

        private void FixedUpdate()
        {
            if (TweenManager.hasActiveFixedTweens && Time.timeScale > 0)
            {
                TweenManager.Update(UpdateType.Fixed, (DoTween.useSmoothDeltaTime ? Time.smoothDeltaTime : Time.deltaTime) * DoTween.timeScale, ((DoTween.useSmoothDeltaTime ? Time.smoothDeltaTime : Time.deltaTime) / Time.timeScale) * DoTween.timeScale);
            }
        }

        // Now activated directly by DOTween so it can be used to run tweens in editor mode
//        public void ManualUpdate(float deltaTime, float unscaledDeltaTime)
//        {
//            if (TweenManager.hasActiveManualTweens) {
//                TweenManager.Update(UpdateType.Manual, deltaTime * DOTween.timeScale, unscaledDeltaTime * DOTween.timeScale);
//            }
//        }

        // Removed to allow compatibility with Unity 5.4 and later
//        void OnLevelWasLoaded()
//        {
//            if (DOTween.useSafeMode) DOTween.Validate();
//        }

        private void OnDrawGizmos()
        {
            if (!DoTween.drawGizmos || !TweenManager.isUnityEditor) return;

            var len = DoTween.GizmosDelegates.Count;
            if (len == 0) return;

            for (var i = 0; i < len; ++i) DoTween.GizmosDelegates[i]();
        }

        private void OnDestroy()
        {
            if (_duplicateToDestroy) return;

            if (DoTween.showUnityEditorReport)
            {
                var s = "Max overall simultaneous active Tweeners/Sequences: " + DoTween.maxActiveTweenersReached + "/" + DoTween.maxActiveSequencesReached;
                Debugger.LogReport(s);
            }

            if (DoTween.useSafeMode)
            {
                var totSafeModeErrors = DoTween.safeModeReport.GetTotErrors();
                if (totSafeModeErrors > 0)
                {
                    var s = $"DOTween's safe mode captured {totSafeModeErrors} errors. This is usually ok (it's what safe mode is there for) but if your game is encountering issues, you should set Log Behaviour to Default in DOTween Utility Panel in order to get detailed, warnings when an error is captured (consider that these errors are always on the user side).";
                    if (DoTween.safeModeReport.TotMissingTargetOrFieldErrors > 0)
                    {
                        s += "\n- " + DoTween.safeModeReport.TotMissingTargetOrFieldErrors + " missing target or field errors";
                    }

                    if (DoTween.safeModeReport.TotStartupErrors > 0)
                    {
                        s += "\n- " + DoTween.safeModeReport.TotStartupErrors + " startup errors";
                    }

                    if (DoTween.safeModeReport.TotCallbackErrors > 0)
                    {
                        s += "\n- " + DoTween.safeModeReport.TotCallbackErrors + " errors inside callbacks (these might be important)";
                    }

                    if (DoTween.safeModeReport.TotUnsetErrors > 0)
                    {
                        s += "\n- " + DoTween.safeModeReport.TotUnsetErrors + " undetermined errors (these might be important)";
                    }

                    Debugger.LogSafeModeReport(s);
                }
            }

//            DOTween.initialized = false;
//            DOTween.instance = null;
            if (DoTween.instance == this) DoTween.instance = null;
            DoTween.Clear(true);
        }

        // Detract/reapply pause time from/to unscaled time
        public void OnApplicationPause(bool pauseStatus)
        {
            if (pauseStatus)
            {
                _pausedTime = Time.realtimeSinceStartup;
            }
            else
            {
                _unscaledTime += Time.realtimeSinceStartup - _pausedTime;
            }
        }

        // Commented this out because it interferes with Unity 2019.3 "no domain reload" experimental playmode
        // (now I clear DOTween completely when the DOTween component is destroyed which allows this to be commented out)
//        void OnApplicationQuit()
//        {
//            DOTween.isQuitting = true;
//        }

        #endregion

        #region Editor

        #endregion

        #region Public Methods

        /// <summary>
        /// Directly sets the current max capacity of Tweeners and Sequences
        /// (meaning how many Tweeners and Sequences can be running at the same time),
        /// so that DOTween doesn't need to automatically increase them in case the max is reached
        /// (which might lead to hiccups when that happens).
        /// Sequences capacity must be less or equal to Tweeners capacity
        /// (if you pass a low Tweener capacity it will be automatically increased to match the Sequence's).
        /// Beware: use this method only when there are no tweens running.
        /// </summary>
        /// <param name="tweenersCapacity">Max Tweeners capacity.
        /// Default: 200</param>
        /// <param name="sequencesCapacity">Max Sequences capacity.
        /// Default: 50</param>
        public IDoTweenInit SetCapacity(int tweenersCapacity, int sequencesCapacity)
        {
            TweenManager.SetCapacities(tweenersCapacity, sequencesCapacity);
            return this;
        }

        #endregion

        #region Yield Coroutines

        // CALLED BY TweenExtensions, creates a coroutine that waits for the tween to be complete (or killed)
        public IEnumerator WaitForCompletion(Tween t)
        {
            while (t.Active && !t.isComplete) yield return null;
        }

        // CALLED BY TweenExtensions, creates a coroutine that waits for the tween to be rewinded (or killed)
        public IEnumerator WaitForRewind(Tween t)
        {
            while (t.Active && (!t.PlayedOnce || t.Position * (t.completedLoops + 1) > 0)) yield return null;
        }

        // CALLED BY TweenExtensions, creates a coroutine that waits for the tween to be killed
        public IEnumerator WaitForKill(Tween t)
        {
            while (t.Active) yield return null;
        }

        // CALLED BY TweenExtensions, creates a coroutine that waits for the tween to reach a given amount of loops (or to be killed)
        public IEnumerator WaitForElapsedLoops(Tween t, int elapsedLoops)
        {
            while (t.Active && t.completedLoops < elapsedLoops) yield return null;
        }

        // CALLED BY TweenExtensions, creates a coroutine that waits for the tween to reach a given time position (or to be killed)
        public IEnumerator WaitForPosition(Tween t, float position)
        {
            while (t.Active && t.Position * (t.completedLoops + 1) < position) yield return null;
        }

        // CALLED BY TweenExtensions, creates a coroutine that waits for the tween to be started (or killed)
        public IEnumerator WaitForStart(Tween t)
        {
            while (t.Active && !t.PlayedOnce) yield return null;
        }

        #endregion

        public static void Create()
        {
            if (DoTween.instance != null) return;

            var go = new GameObject("[DOTween]");
            DontDestroyOnLoad(go);
            DoTween.instance = go.AddComponent<DoTweenComponent>();
        }

        public static void DestroyInstance()
        {
            if (DoTween.instance != null) Destroy(DoTween.instance.gameObject);
            DoTween.instance = null;
        }
    }
}