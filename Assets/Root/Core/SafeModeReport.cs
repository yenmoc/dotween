﻿// Author: Daniele Giardini - http://www.demigiant.com
// Created: 2019/07/24 13:45
// License Copyright (c) Daniele Giardini
// This work is subject to the terms at http://dotween.demigiant.com/license.php

namespace DG.Tweening.Core
{
    public struct SafeModeReport
    {
        public enum SafeModeReportType
        {
            Unset,
            TargetOrFieldMissing,
            Callback,
            StartupFailure
        }

        public int TotMissingTargetOrFieldErrors { get; private set; }
        public int TotCallbackErrors { get; private set; }
        public int TotStartupErrors { get; private set; }
        public int TotUnsetErrors { get; private set; }

        public void Add(SafeModeReportType type)
        {
            switch (type) {
            case SafeModeReportType.TargetOrFieldMissing:
                TotMissingTargetOrFieldErrors++;
                break;
            case SafeModeReportType.Callback:
                TotCallbackErrors++;
                break;
            case SafeModeReportType.StartupFailure:
                TotStartupErrors++;
                break;
            default:
                TotUnsetErrors++;
                break;
            }
        }

        public int GetTotErrors()
        {
            return TotMissingTargetOrFieldErrors + TotCallbackErrors + TotStartupErrors + TotUnsetErrors;
        }
    }
}