﻿// Author: Daniele Giardini - http://www.demigiant.com
// Created: 2014/05/07 13:00
// 
// License Copyright (c) Daniele Giardini.
// This work is subject to the terms at http://dotween.demigiant.com/license.php

using System;
using System.Collections.Generic;
using DG.Tweening.Core.Enums;
using DG.Tweening.Plugins.Options;
using UnityEngine;

// ReSharper disable once CheckNamespace
namespace DG.Tweening.Core
{
    public static class TweenManager
    {
        private const int DEFAULT_MAX_TWEENERS = 200;
        private const int DEFAULT_MAX_SEQUENCES = 50;
        private const string MAX_TWEENS_REACHED = "Max Tweens reached: capacity has automatically been increased from #0 to #1. Use DOTween.SetTweensCapacity to set it manually at startup";
        private const float EPSILON_VS_TIME_CHECK = 0.000001f;

        public static bool isUnityEditor;
        public static bool isDebugBuild;
        public static int maxActive = DEFAULT_MAX_TWEENERS + DEFAULT_MAX_SEQUENCES; // Always equal to maxTweeners + maxSequences
        public static int maxTweeners = DEFAULT_MAX_TWEENERS; // Always >= maxSequences
        public static int maxSequences = DEFAULT_MAX_SEQUENCES; // Always <= maxTweeners
        public static bool hasActiveTweens, hasActiveDefaultTweens, hasActiveLateTweens, hasActiveFixedTweens, hasActiveManualTweens;
        public static int totActiveTweens, totActiveDefaultTweens, totActiveLateTweens, totActiveFixedTweens, totActiveManualTweens;
        public static int totActiveTweeners, totActiveSequences;
        public static int totPooledTweeners, totPooledSequences;
        public static int totTweeners, totSequences; // Both active and pooled
        public static bool isUpdateLoop; // TRUE while an update cycle is running (used to treat direct tween Kills differently)

        // Tweens contained in Sequences are not inside the active lists
        // Arrays are organized (max once per update) so that existing elements are next to each other from 0 to (totActiveTweens - 1)
        public static Tween[] activeTweens = new Tween[DEFAULT_MAX_TWEENERS + DEFAULT_MAX_SEQUENCES]; // public just to allow DOTweenInspector to access it
        private static Tween[] pooledTweeners = new Tween[DEFAULT_MAX_TWEENERS];
        private static readonly Stack<Tween> PooledSequences = new Stack<Tween>();

        private static readonly List<Tween> KillList = new List<Tween>(DEFAULT_MAX_TWEENERS + DEFAULT_MAX_SEQUENCES);
        private static readonly Dictionary<Tween, TweenLink> TweenLinks = new Dictionary<Tween, TweenLink>(DEFAULT_MAX_TWEENERS + DEFAULT_MAX_SEQUENCES);
        private static int totTweenLinks; // Used for quicker skip in case no TweenLinks were set
        private static int maxActiveLookupId = -1; // Highest full ID in _activeTweens
        private static bool requiresActiveReorganization; // True when _activeTweens need to be reorganized to fill empty spaces
        private static int reorganizeFromId = -1; // First null ID from which to reorganize
        private static int minPooledTweenerId = -1; // Lowest PooledTweeners id that is actually full
        private static int maxPooledTweenerId = -1; // Highest PooledTweeners id that is actually full

        // Used to prevent tweens from being re-killed at the end of an update loop if KillAll was called during said loop
        private static bool despawnAllCalledFromUpdateLoopCallback;

#if DEBUG
        public static int updateLoopCount;
#endif

        #region Static Constructor

        static TweenManager()
        {
            isUnityEditor = Application.isEditor;
#if DEBUG
            isDebugBuild = true;
#endif
        }

        #endregion

        #region Main

        // Returns a new Tweener, from the pool if there's one available,
        // otherwise by instantiating a new one
        public static TweenerCore<T1, T2, TPlugOptions> GetTweener<T1, T2, TPlugOptions>()
            where TPlugOptions : struct, IPlugOptions
        {
            TweenerCore<T1, T2, TPlugOptions> t;
            // Search inside pool
            if (totPooledTweeners > 0)
            {
                var typeofT1 = typeof(T1);
                var typeofT2 = typeof(T2);
                var typeofTPlugOptions = typeof(TPlugOptions);
                for (var i = maxPooledTweenerId; i > minPooledTweenerId - 1; --i)
                {
                    var tween = pooledTweeners[i];
                    if (tween != null && tween.typeofT1 == typeofT1 && tween.typeofT2 == typeofT2 && tween.typeofTPlugOptions == typeofTPlugOptions)
                    {
                        // Pooled Tweener exists: spawn it
                        t = (TweenerCore<T1, T2, TPlugOptions>) tween;
                        AddActiveTween(t);
                        pooledTweeners[i] = null;
                        if (maxPooledTweenerId != minPooledTweenerId)
                        {
                            if (i == maxPooledTweenerId) maxPooledTweenerId--;
                            else if (i == minPooledTweenerId) minPooledTweenerId++;
                        }

                        totPooledTweeners--;
                        return t;
                    }
                }

                // Not found: remove a tween from the pool in case it's full
                if (totTweeners >= maxTweeners)
                {
                    pooledTweeners[maxPooledTweenerId] = null;
                    maxPooledTweenerId--;
                    totPooledTweeners--;
                    totTweeners--;
                }
            }
            else
            {
                // Increase capacity in case max number of Tweeners has already been reached, then continue
                if (totTweeners >= maxTweeners - 1)
                {
                    var prevMaxTweeners = maxTweeners;
                    var prevMaxSequences = maxSequences;
                    IncreaseCapacities(CapacityIncreaseMode.TweenersOnly);
                    if (Debugger.LogPriority >= 1)
                        Debugger.LogWarning(MAX_TWEENS_REACHED
                            .Replace("#0", prevMaxTweeners + "/" + prevMaxSequences)
                            .Replace("#1", maxTweeners + "/" + maxSequences)
                        );
                }
            }

            // Not found: create new TweenerController
            t = new TweenerCore<T1, T2, TPlugOptions>();
            totTweeners++;
            AddActiveTween(t);
            return t;
        }

        // Returns a new Sequence, from the pool if there's one available,
        // otherwise by instantiating a new one
        public static Sequence GetSequence()
        {
            Sequence s;
            if (totPooledSequences > 0)
            {
                s = (Sequence) PooledSequences.Pop();
                AddActiveTween(s);
                totPooledSequences--;
                return s;
            }

            // Increase capacity in case max number of Sequences has already been reached, then continue
            if (totSequences >= maxSequences - 1)
            {
                var prevMaxTweeners = maxTweeners;
                var prevMaxSequences = maxSequences;
                IncreaseCapacities(CapacityIncreaseMode.SequencesOnly);
                if (Debugger.LogPriority >= 1)
                    Debugger.LogWarning(MAX_TWEENS_REACHED
                        .Replace("#0", prevMaxTweeners + "/" + prevMaxSequences)
                        .Replace("#1", maxTweeners + "/" + maxSequences)
                    );
            }

            // Not found: create new Sequence
            s = new Sequence();
            totSequences++;
            AddActiveTween(s);
            return s;
        }

        public static void SetUpdateType(Tween t, UpdateType updateType, bool isIndependentUpdate)
        {
            if (!t.Active || t.updateType == updateType)
            {
                t.updateType = updateType;
                t.isIndependentUpdate = isIndependentUpdate;
                return;
            }

            // Remove previous update type
            if (t.updateType == UpdateType.Normal)
            {
                totActiveDefaultTweens--;
                hasActiveDefaultTweens = totActiveDefaultTweens > 0;
            }
            else
            {
                switch (t.updateType)
                {
                    case UpdateType.Fixed:
                        totActiveFixedTweens--;
                        hasActiveFixedTweens = totActiveFixedTweens > 0;
                        break;
                    case UpdateType.Late:
                        totActiveLateTweens--;
                        hasActiveLateTweens = totActiveLateTweens > 0;
                        break;
                    default: // Manual
                        totActiveManualTweens--;
                        hasActiveManualTweens = totActiveManualTweens > 0;
                        break;
                }
            }

            // Assign new one
            t.updateType = updateType;
            t.isIndependentUpdate = isIndependentUpdate;
            if (updateType == UpdateType.Normal)
            {
                totActiveDefaultTweens++;
                hasActiveDefaultTweens = true;
            }
            else
            {
                switch (updateType)
                {
                    case UpdateType.Fixed:
                        totActiveFixedTweens++;
                        hasActiveFixedTweens = true;
                        break;
                    case UpdateType.Late:
                        totActiveLateTweens++;
                        hasActiveLateTweens = true;
                        break;
                    default: // Manual
                        totActiveManualTweens++;
                        hasActiveManualTweens = true;
                        break;
                }
            }
        }

        // Removes the given tween from the active tweens list
        public static void AddActiveTweenToSequence(Tween t)
        {
            RemoveActiveTween(t);
        }

        // Despawn all
        public static int DespawnAll()
        {
            var totDespawned = totActiveTweens;
            for (var i = 0; i < maxActiveLookupId + 1; ++i)
            {
                var t = activeTweens[i];
                if (t != null) Despawn(t, false);
            }

            ClearTweenArray(activeTweens);
            hasActiveTweens = hasActiveDefaultTweens = hasActiveLateTweens = hasActiveFixedTweens = hasActiveManualTweens = false;
            totActiveTweens = totActiveDefaultTweens = totActiveLateTweens = totActiveFixedTweens = totActiveManualTweens = 0;
            totActiveTweeners = totActiveSequences = 0;
            maxActiveLookupId = reorganizeFromId = -1;
            requiresActiveReorganization = false;
            TweenLinks.Clear();
            totTweenLinks = 0;

            if (isUpdateLoop) despawnAllCalledFromUpdateLoopCallback = true;

            return totDespawned;
        }

        public static void Despawn(Tween t, bool modifyActiveLists = true)
        {
            // Callbacks
            if (t.onKill != null) Tween.OnTweenCallback(t.onKill);

            if (modifyActiveLists)
            {
                // Remove tween from active list
                RemoveActiveTween(t);
            }

            if (t.isRecyclable)
            {
                // Put the tween inside a pool
                switch (t.tweenType)
                {
                    case TweenType.Sequence:
                        PooledSequences.Push(t);
                        totPooledSequences++;
                        // Despawn sequenced tweens
                        var s = (Sequence) t;
                        var len = s.sequencedTweens.Count;
                        for (var i = 0; i < len; ++i) Despawn(s.sequencedTweens[i], false);
                        break;
                    case TweenType.Tweener:
                        if (maxPooledTweenerId == -1)
                        {
                            maxPooledTweenerId = maxTweeners - 1;
                            minPooledTweenerId = maxTweeners - 1;
                        }

                        if (maxPooledTweenerId < maxTweeners - 1)
                        {
                            pooledTweeners[maxPooledTweenerId + 1] = t;
                            maxPooledTweenerId++;
                            if (minPooledTweenerId > maxPooledTweenerId) minPooledTweenerId = maxPooledTweenerId;
                        }
                        else
                        {
                            for (var i = maxPooledTweenerId; i > -1; --i)
                            {
                                if (pooledTweeners[i] != null) continue;
                                pooledTweeners[i] = t;
                                if (i < minPooledTweenerId) minPooledTweenerId = i;
                                if (maxPooledTweenerId < minPooledTweenerId) maxPooledTweenerId = minPooledTweenerId;
                                break;
                            }
                        }

                        totPooledTweeners++;
                        break;
                }
            }
            else
            {
                // Remove
                switch (t.tweenType)
                {
                    case TweenType.Sequence:
                        totSequences--;
                        // Despawn sequenced tweens
                        var s = (Sequence) t;
                        var len = s.sequencedTweens.Count;
                        for (var i = 0; i < len; ++i) Despawn(s.sequencedTweens[i], false);
                        break;
                    case TweenType.Tweener:
                        totTweeners--;
                        break;
                }
            }

            t.Active = false;
            t.Reset();
        }

        // Destroys any active tween without putting them back in a pool,
        // then purges all pools and resets capacities
        public static void PurgeAll()
        {
            // Fire eventual onKill callbacks
            for (var i = 0; i < totActiveTweens; ++i)
            {
                var t = activeTweens[i];
                if (t != null)
                {
                    t.Active = false;
                    if (t.onKill != null) Tween.OnTweenCallback(t.onKill);
                }
            }

            ClearTweenArray(activeTweens);
            hasActiveTweens = hasActiveDefaultTweens = hasActiveLateTweens = hasActiveFixedTweens = hasActiveManualTweens = false;
            totActiveTweens = totActiveDefaultTweens = totActiveLateTweens = totActiveFixedTweens = totActiveManualTweens = 0;
            totActiveTweeners = totActiveSequences = 0;
            maxActiveLookupId = reorganizeFromId = -1;
            requiresActiveReorganization = false;
            PurgePools();
            ResetCapacities();
            totTweeners = totSequences = 0;
        }

        // Removes any cached tween from the pools
        public static void PurgePools()
        {
            totTweeners -= totPooledTweeners;
            totSequences -= totPooledSequences;
            ClearTweenArray(pooledTweeners);
            PooledSequences.Clear();
            totPooledTweeners = totPooledSequences = 0;
            minPooledTweenerId = maxPooledTweenerId = -1;
        }

        public static void AddTweenLink(Tween t, TweenLink tweenLink)
        {
            totTweenLinks++;
            if (TweenLinks.ContainsKey(t)) TweenLinks[t] = tweenLink;
            else TweenLinks.Add(t, tweenLink);
            // Pause or play tween immediately depending on target's state
            if (tweenLink.lastSeenActive)
            {
                switch (tweenLink.behaviour)
                {
                    case LinkBehaviour.PauseOnDisablePlayOnEnable:
                    case LinkBehaviour.PauseOnDisableRestartOnEnable:
                    case LinkBehaviour.PlayOnEnable:
                    case LinkBehaviour.RestartOnEnable:
                        Play(t);
                        break;
                }
            }
            else
            {
                switch (tweenLink.behaviour)
                {
                    case LinkBehaviour.PauseOnDisable:
                    case LinkBehaviour.PauseOnDisablePlayOnEnable:
                    case LinkBehaviour.PauseOnDisableRestartOnEnable:
                        Pause(t);
                        break;
                }
            }
        }

        private static void RemoveTweenLink(Tween t)
        {
            if (!TweenLinks.ContainsKey(t)) return;
            TweenLinks.Remove(t);
            totTweenLinks--;
        }

        public static void ResetCapacities()
        {
            SetCapacities(DEFAULT_MAX_TWEENERS, DEFAULT_MAX_SEQUENCES);
        }

        public static void SetCapacities(int tweenersCapacity, int sequencesCapacity)
        {
            if (tweenersCapacity < sequencesCapacity) tweenersCapacity = sequencesCapacity;

//            maxActive = tweenersCapacity;
            maxActive = tweenersCapacity + sequencesCapacity;
            maxTweeners = tweenersCapacity;
            maxSequences = sequencesCapacity;
            Array.Resize(ref activeTweens, maxActive);
            Array.Resize(ref pooledTweeners, tweenersCapacity);
            KillList.Capacity = maxActive;
        }

        // Looks through all active tweens and removes the ones whose getters generate errors
        // (usually meaning their target has become NULL).
        // Returns the total number of invalid tweens found and removed
        // BEWARE: this is an expensive operation
        public static int Validate()
        {
            if (requiresActiveReorganization) ReorganizeActiveTweens();

            var totInvalid = 0;
            for (var i = 0; i < maxActiveLookupId + 1; ++i)
            {
                var t = activeTweens[i];
                if (!t.Validate())
                {
                    totInvalid++;
                    MarkForKilling(t);
                }
            }

            // Kill all eventually marked tweens
            if (totInvalid > 0)
            {
                DespawnActiveTweens(KillList);
                KillList.Clear();
            }

            return totInvalid;
        }

        // deltaTime will be passed as fixedDeltaTime in case of UpdateType.Fixed
        public static void Update(UpdateType updateType, float deltaTime, float independentTime)
        {
            if (requiresActiveReorganization) ReorganizeActiveTweens();

            isUpdateLoop = true;
#if DEBUG
            updateLoopCount++;
            VerifyActiveTweensList();
#endif
            var willKill = false;
//            Debug.Log("::::::::::: " + updateType + " > " + (_maxActiveLookupId + 1));
            var len = maxActiveLookupId + 1; // Stored here so if _maxActiveLookupId changed during update loop (like if new tween is created at onComplete) new tweens are still ignored
            for (var i = 0; i < len; ++i)
            {
                var t = activeTweens[i];
                if (t == null || t.updateType != updateType) continue; // Wrong updateType or was added to a Sequence (thus removed from active list) while inside current updateLoop
                if (totTweenLinks > 0) EvaluateTweenLink(t); // TweenLinks
                if (!t.Active)
                {
                    // Manually killed by another tween's callback or deactivated by the TweenLink evaluation
                    willKill = true;
                    MarkForKilling(t);
                    continue;
                }

                if (!t.isPlaying) continue;
                t.creationLocked = true; // Lock tween creation methods from now on
                var tDeltaTime = (t.isIndependentUpdate ? independentTime : deltaTime) * t.timeScale;
//                if (tDeltaTime <= 0) continue; // Skip update in case time is 0 (commented in favor of next line because this prevents negative timeScales)
                if (tDeltaTime < EPSILON_VS_TIME_CHECK && tDeltaTime > -EPSILON_VS_TIME_CHECK) continue; // Skip update in case time is approximately 0
                if (!t.delayComplete)
                {
                    tDeltaTime = t.UpdateDelay(t.elapsedDelay + tDeltaTime);
                    if (tDeltaTime <= -1)
                    {
                        // Error during startup (can happen with FROM tweens): mark tween for killing
                        willKill = true;
                        MarkForKilling(t);
                        continue;
                    }

                    if (tDeltaTime <= 0) continue;
                    // Delay elapsed - call OnPlay if required
                    if (t.PlayedOnce && t.onPlay != null)
                    {
                        // Don't call in case it hasn't started because onStart routine will call it
                        Tween.OnTweenCallback(t.onPlay);
                    }
                }

                // Startup (needs to be here other than in Tween.DoGoto in case of speed-based tweens, to calculate duration correctly)
                if (!t.startupDone)
                {
                    if (!t.Startup())
                    {
                        // Startup failure: mark for killing
                        willKill = true;
                        MarkForKilling(t);
                        continue;
                    }
                }

                // Find update data
                var toPosition = t.Position;
                var wasEndPosition = toPosition >= t.duration;
                var toCompletedLoops = t.completedLoops;
                if (t.duration <= 0)
                {
                    toPosition = 0;
                    toCompletedLoops = t.loops == -1 ? t.completedLoops + 1 : t.loops;
                }
                else
                {
                    if (t.isBackwards)
                    {
                        toPosition -= tDeltaTime;
                        while (toPosition < 0 && toCompletedLoops > -1)
                        {
                            toPosition += t.duration;
                            toCompletedLoops--;
                        }

                        if (toCompletedLoops < 0 || wasEndPosition && toCompletedLoops < 1)
                        {
                            // Result is equivalent to a rewind, so set values according to it
                            toPosition = 0;
                            toCompletedLoops = wasEndPosition ? 1 : 0;
                        }

//                        while (toPosition < 0 && toCompletedLoops > 0) {
//                            toPosition += t.duration;
//                            toCompletedLoops--;
//                        }
//                        if (wasEndPosition && toCompletedLoops <= 0) {
//                            // Force-rewind
//                            Rewind(t, false);
//                            continue;
//                        }
                    }
                    else
                    {
                        toPosition += tDeltaTime;
                        while (toPosition >= t.duration && (t.loops == -1 || toCompletedLoops < t.loops))
                        {
                            toPosition -= t.duration;
                            toCompletedLoops++;
                        }
                    }

                    if (wasEndPosition) toCompletedLoops--;
                    if (t.loops != -1 && toCompletedLoops >= t.loops) toPosition = t.duration;
                }

                // Goto
                var needsKilling = Tween.DoGoto(t, toPosition, toCompletedLoops, UpdateMode.Update);
                if (needsKilling)
                {
                    willKill = true;
                    MarkForKilling(t);
                }
            }

            // Kill all eventually marked tweens
            if (willKill)
            {
                if (despawnAllCalledFromUpdateLoopCallback)
                {
                    // Do not despawn tweens again, since Kill/DespawnAll was already called
                    despawnAllCalledFromUpdateLoopCallback = false;
                }
                else
                {
                    DespawnActiveTweens(KillList);
                }

                KillList.Clear();
            }

            isUpdateLoop = false;
        }

        public static int FilteredOperation(OperationType operationType, FilterType filterType, object id, bool optionalBool, float optionalFloat, object optionalObj = null, object[] optionalArray = null)
        {
            var totInvolved = 0;
            var hasDespawned = false;
            var optionalArrayLen = optionalArray == null ? 0 : optionalArray.Length;
            // Determine if ID is required, and if it's stringId
            var useStringId = false;
            string stringId = null;
            var useIntId = false;
            var intId = 0;
            switch (filterType)
            {
                case FilterType.TargetOrId:
                case FilterType.TargetAndId:
                    if (id is string)
                    {
                        useStringId = true;
                        stringId = (string) id;
                    }
                    else if (id is int)
                    {
                        useIntId = true;
                        intId = (int) id;
                    }

                    break;
            }

            for (var i = maxActiveLookupId; i > -1; --i)
            {
                var t = activeTweens[i];
                if (t == null || !t.Active) continue;

                var isFilterCompliant = false;
                switch (filterType)
                {
                    case FilterType.All:
                        isFilterCompliant = true;
                        break;
                    case FilterType.TargetOrId:
                        if (useStringId) isFilterCompliant = t.stringId != null && t.stringId == stringId;
                        else if (useIntId) isFilterCompliant = t.intId == intId;
                        else isFilterCompliant = t.id != null && id.Equals(t.id) || t.target != null && id.Equals(t.target);
                        break;
                    case FilterType.TargetAndId:
                        if (useStringId) isFilterCompliant = t.target != null && t.stringId == stringId && optionalObj != null && optionalObj.Equals(t.target);
                        else if (useIntId) isFilterCompliant = t.target != null && t.intId == intId && optionalObj != null && optionalObj.Equals(t.target);
                        else isFilterCompliant = t.id != null && t.target != null && optionalObj != null && id.Equals(t.id) && optionalObj.Equals(t.target);
                        break;
                    case FilterType.AllExceptTargetsOrIds:
                        isFilterCompliant = true;
                        for (var c = 0; c < optionalArrayLen; ++c)
                        {
                            var objId = optionalArray[c];
                            if (objId is string)
                            {
                                useStringId = true;
                                stringId = (string) objId;
                            }
                            else if (objId is int)
                            {
                                useIntId = true;
                                intId = (int) objId;
                            }

                            if (useStringId && t.stringId == stringId)
                            {
                                isFilterCompliant = false;
                                break;
                            }
                            else if (useIntId && t.intId == intId)
                            {
                                isFilterCompliant = false;
                                break;
                            }
                            else if (t.id != null && objId.Equals(t.id) || t.target != null && objId.Equals(t.target))
                            {
                                isFilterCompliant = false;
                                break;
                            }
                        }

                        break;
                }

                if (isFilterCompliant)
                {
                    switch (operationType)
                    {
                        case OperationType.Despawn:
                            totInvolved++;
                            t.Active = false; // Mark it as inactive immediately, so eventual kills called inside a kill won't have effect
//                        if (isUpdateLoop) t.active = false; // Just mark it for killing, so the update loop will take care of it
                            if (isUpdateLoop) break; // Just mark it for killing, the update loop will take care of the rest
                            Despawn(t, false);
                            hasDespawned = true;
                            KillList.Add(t);
                            break;
                        case OperationType.Complete:
                            var hasAutoKill = t.autoKill;
                            // If optionalFloat is > 0 completes with callbacks
                            if (Complete(t, false, optionalFloat > 0 ? UpdateMode.Update : UpdateMode.Goto))
                            {
                                // If optionalBool is TRUE only returns tweens killed by completion
                                totInvolved += !optionalBool ? 1 : hasAutoKill ? 1 : 0;
                                if (hasAutoKill)
                                {
                                    if (isUpdateLoop) t.Active = false; // Just mark it for killing, so the update loop will take care of it
                                    else
                                    {
                                        hasDespawned = true;
                                        KillList.Add(t);
                                    }
                                }
                            }

                            break;
                        case OperationType.Flip:
                            if (Flip(t)) totInvolved++;
                            break;
                        case OperationType.Goto:
                            Goto(t, optionalFloat, optionalBool);
                            totInvolved++;
                            break;
                        case OperationType.Pause:
                            if (Pause(t)) totInvolved++;
                            break;
                        case OperationType.Play:
                            if (Play(t)) totInvolved++;
                            break;
                        case OperationType.PlayBackwards:
                            if (PlayBackwards(t)) totInvolved++;
                            break;
                        case OperationType.PlayForward:
                            if (PlayForward(t)) totInvolved++;
                            break;
                        case OperationType.Restart:
                            if (Restart(t, optionalBool, optionalFloat)) totInvolved++;
                            break;
                        case OperationType.Rewind:
                            if (Rewind(t, optionalBool)) totInvolved++;
                            break;
                        case OperationType.SmoothRewind:
                            if (SmoothRewind(t)) totInvolved++;
                            break;
                        case OperationType.TogglePause:
                            if (TogglePause(t)) totInvolved++;
                            break;
                        case OperationType.IsTweening:
                            if ((!t.isComplete || !t.autoKill) && (!optionalBool || t.isPlaying)) totInvolved++;
                            break;
                    }
                }
            }

            // Special additional operations in case of despawn
            if (hasDespawned)
            {
                var count = KillList.Count - 1;
                for (var i = count; i > -1; --i)
                {
                    var t = KillList[i];
                    // Ignore tweens with activeId -1, since they were already killed and removed
                    //  by nested OnComplete callbacks
                    if (t.activeId != -1) RemoveActiveTween(t);
                }

                KillList.Clear();
            }

            return totInvolved;
        }

        #endregion

        #region Play Operations

        public static bool Complete(Tween t, bool modifyActiveLists = true, UpdateMode updateMode = UpdateMode.Goto)
        {
            if (t.loops == -1) return false;
            if (!t.isComplete)
            {
                Tween.DoGoto(t, t.duration, t.loops, updateMode);
                t.isPlaying = false;
                // Despawn if needed
                if (t.autoKill)
                {
                    if (isUpdateLoop) t.Active = false; // Just mark it for killing, so the update loop will take care of it
                    else Despawn(t, modifyActiveLists);
                }

                return true;
            }

            return false;
        }

        public static bool Flip(Tween t)
        {
            t.isBackwards = !t.isBackwards;
            return true;
        }

        // Forces the tween to startup and initialize all its data
        public static void ForceInit(Tween t, bool isSequenced = false)
        {
            if (t.startupDone) return;

            if (!t.Startup() && !isSequenced)
            {
                // Startup failed: kill tween
                if (isUpdateLoop) t.Active = false; // Just mark it for killing, so the update loop will take care of it
                else RemoveActiveTween(t);
            }
        }

        // Returns TRUE if there was an error and the tween needs to be destroyed
        public static bool Goto(Tween t, float to, bool andPlay = false, UpdateMode updateMode = UpdateMode.Goto)
        {
            var wasPlaying = t.isPlaying;
            t.isPlaying = andPlay;
            t.delayComplete = true;
            t.elapsedDelay = t.delay;
//            int toCompletedLoops = (int)(to / t.duration); // With very small floats creates floating points imprecisions
            var toCompletedLoops = t.duration <= 0 ? 1 : Mathf.FloorToInt(to / t.duration); // Still generates imprecision with some values (like 0.4)
//            int toCompletedLoops = (int)((decimal)to / (decimal)t.duration); // Takes care of floating points imprecision (nahh doesn't work correctly either)
            var toPosition = to % t.duration;
            if (t.loops != -1 && toCompletedLoops >= t.loops)
            {
                toCompletedLoops = t.loops;
                toPosition = t.duration;
            }
            else if (toPosition >= t.duration) toPosition = 0;

            // If andPlay is FALSE manage onPause from here because DoGoto won't detect it (since t.isPlaying was already set from here)
            var needsKilling = Tween.DoGoto(t, toPosition, toCompletedLoops, updateMode);
            if (!andPlay && wasPlaying && !needsKilling && t.onPause != null) Tween.OnTweenCallback(t.onPause);
            return needsKilling;
        }

        // Returns TRUE if the given tween was not already paused
        public static bool Pause(Tween t)
        {
            if (t.isPlaying)
            {
                t.isPlaying = false;
                if (t.onPause != null) Tween.OnTweenCallback(t.onPause);
                return true;
            }

            return false;
        }

        // Returns TRUE if the given tween was not already playing and is not complete
        public static bool Play(Tween t)
        {
            if (!t.isPlaying && (!t.isBackwards && !t.isComplete || t.isBackwards && (t.completedLoops > 0 || t.Position > 0)))
            {
                t.isPlaying = true;
                if (t.PlayedOnce && t.delayComplete && t.onPlay != null)
                {
                    // Don't call in case there's a delay to run or if it hasn't started because onStart routine will call it
                    Tween.OnTweenCallback(t.onPlay);
                }

                return true;
            }

            return false;
        }

        public static bool PlayBackwards(Tween t)
        {
            if (t.completedLoops == 0 && t.Position <= 0)
            {
                // Already rewinded, manage OnRewind callback
                ManageOnRewindCallbackWhenAlreadyRewinded(t, true);
                t.isBackwards = true;
                t.isPlaying = false;
                return false;
            }

            if (!t.isBackwards)
            {
                t.isBackwards = true;
                Play(t);
                return true;
            }

            return Play(t);
        }

        public static bool PlayForward(Tween t)
        {
            if (t.isComplete)
            {
                t.isBackwards = false;
                t.isPlaying = false;
                return false;
            }

            if (t.isBackwards)
            {
                t.isBackwards = false;
                Play(t);
                return true;
            }

            return Play(t);
        }

        public static bool Restart(Tween t, bool includeDelay = true, float changeDelayTo = -1)
        {
            var wasPaused = !t.isPlaying;
            t.isBackwards = false;
            if (changeDelayTo >= 0) t.delay = changeDelayTo;
            Rewind(t, includeDelay);
            t.isPlaying = true;
            if (wasPaused && t.PlayedOnce && t.delayComplete && t.onPlay != null)
            {
                // Don't call in case there's a delay to run or if it hasn't started because onStart routine will call it
                Tween.OnTweenCallback(t.onPlay);
            }

            return true;
        }

        public static bool Rewind(Tween t, bool includeDelay = true)
        {
            var wasPlaying = t.isPlaying; // Manage onPause from this method because DoGoto won't detect it
            t.isPlaying = false;
            var rewinded = false;
            if (t.delay > 0)
            {
                if (includeDelay)
                {
                    rewinded = t.delay > 0 && t.elapsedDelay > 0;
                    t.elapsedDelay = 0;
                    t.delayComplete = false;
                }
                else
                {
                    rewinded = t.elapsedDelay < t.delay;
                    t.elapsedDelay = t.delay;
                    t.delayComplete = true;
                }
            }

            if (t.Position > 0 || t.completedLoops > 0 || !t.startupDone)
            {
                rewinded = true;
                var needsKilling = Tween.DoGoto(t, 0, 0, UpdateMode.Goto);
                if (!needsKilling && wasPlaying && t.onPause != null) Tween.OnTweenCallback(t.onPause);
            }
            else
            {
                // Alread rewinded
                ManageOnRewindCallbackWhenAlreadyRewinded(t, false);
            }

            return rewinded;
        }

        public static bool SmoothRewind(Tween t)
        {
            var rewinded = false;
            if (t.delay > 0)
            {
                rewinded = t.elapsedDelay < t.delay;
                t.elapsedDelay = t.delay;
                t.delayComplete = true;
            }

            if (t.Position > 0 || t.completedLoops > 0 || !t.startupDone)
            {
                rewinded = true;
                if (t.loopType == LoopType.Incremental) t.PlayBackwards();
                else
                {
                    t.Goto(t.ElapsedDirectionalPercentage() * t.duration);
                    t.PlayBackwards();
                }
            }
            else
            {
                // Already rewinded
                t.isPlaying = false;
                ManageOnRewindCallbackWhenAlreadyRewinded(t, true);
            }

            return rewinded;
        }

        public static bool TogglePause(Tween t)
        {
            if (t.isPlaying) return Pause(t);
            return Play(t);
        }

        #endregion

        #region Info Getters

        public static int TotalPooledTweens()
        {
            return totPooledTweeners + totPooledSequences;
        }

        public static int TotalPlayingTweens()
        {
            if (!hasActiveTweens) return 0;

            if (requiresActiveReorganization) ReorganizeActiveTweens();

            var tot = 0;
            for (var i = 0; i < maxActiveLookupId + 1; ++i)
            {
                var t = activeTweens[i];
                if (t != null && t.isPlaying) tot++;
            }

            return tot;
        }

        // If playing is FALSE returns active paused tweens, otherwise active playing tweens
        public static List<Tween> GetActiveTweens(bool playing, List<Tween> fillableList = null)
        {
            if (requiresActiveReorganization) ReorganizeActiveTweens();

            if (totActiveTweens <= 0) return null;
            var len = totActiveTweens;
            if (fillableList == null) fillableList = new List<Tween>(len);
            for (var i = 0; i < len; ++i)
            {
                var t = activeTweens[i];
                if (t.isPlaying == playing) fillableList.Add(t);
            }

            if (fillableList.Count > 0) return fillableList;
            return null;
        }

        // Returns all active tweens with the given id
        public static List<Tween> GetTweensById(object id, bool playingOnly, List<Tween> fillableList = null)
        {
            if (requiresActiveReorganization) ReorganizeActiveTweens();

            if (totActiveTweens <= 0) return null;
            var len = totActiveTweens;
            if (fillableList == null) fillableList = new List<Tween>(len);
            // Determine ID to use
            var useStringId = false;
            string stringId = null;
            var useIntId = false;
            var intId = 0;
            if (id is string)
            {
                useStringId = true;
                stringId = (string) id;
            }
            else if (id is int)
            {
                useIntId = true;
                intId = (int) id;
            }

            //
            for (var i = 0; i < len; ++i)
            {
                var t = activeTweens[i];
                if (t == null) continue;
                if (useStringId)
                {
                    if (t.stringId == null || t.stringId != stringId) continue;
                }
                else if (useIntId)
                {
                    if (t.intId != intId) continue;
                }
                else if (t.id == null || !Equals(id, t.id)) continue;

                if (!playingOnly || t.isPlaying) fillableList.Add(t);
            }

            if (fillableList.Count > 0) return fillableList;
            return null;
        }

        // Returns all active tweens with the given target
        public static List<Tween> GetTweensByTarget(object target, bool playingOnly, List<Tween> fillableList = null)
        {
            if (requiresActiveReorganization) ReorganizeActiveTweens();

            if (totActiveTweens <= 0) return null;
            var len = totActiveTweens;
            if (fillableList == null) fillableList = new List<Tween>(len);
            for (var i = 0; i < len; ++i)
            {
                var t = activeTweens[i];
                if (t.target != target) continue;
                if (!playingOnly || t.isPlaying) fillableList.Add(t);
            }

            if (fillableList.Count > 0) return fillableList;
            return null;
        }

        #endregion

        #region Private Methods

        private static void MarkForKilling(Tween t)
        {
            t.Active = false;
            KillList.Add(t);
        }

        // Called by Update method
        private static void EvaluateTweenLink(Tween t)
        {
            // Check tween links
            TweenLink tLink;
            if (!TweenLinks.TryGetValue(t, out tLink)) return;

            if (tLink.target == null)
            {
                t.Active = false;
            }
            else
            {
                var goActive = tLink.target.activeInHierarchy;
                var justEnabled = !tLink.lastSeenActive && goActive;
                var justDisabled = tLink.lastSeenActive && !goActive;
                tLink.lastSeenActive = goActive;
                switch (tLink.behaviour)
                {
                    case LinkBehaviour.KillOnDisable:
                        if (!goActive) t.Active = false; // Will be killed by rest of Update loop
                        break;
                    case LinkBehaviour.PauseOnDisable:
                        if (justDisabled && t.isPlaying) Pause(t);
                        break;
                    case LinkBehaviour.PauseOnDisablePlayOnEnable:
                        if (justDisabled) Pause(t);
                        else if (justEnabled) Play(t);
                        break;
                    case LinkBehaviour.PauseOnDisableRestartOnEnable:
                        if (justDisabled) Pause(t);
                        else if (justEnabled) Restart(t);
                        break;
                    case LinkBehaviour.PlayOnEnable:
                        if (justEnabled) Play(t);
                        break;
                    case LinkBehaviour.RestartOnEnable:
                        if (justEnabled) Restart(t);
                        break;
                }
            }
        }

        // Adds the given tween to the active tweens list (updateType is always Normal, but can be changed by SetUpdateType)
        private static void AddActiveTween(Tween t)
        {
            if (requiresActiveReorganization) ReorganizeActiveTweens();

            // Safety check (IndexOutOfRangeException)
            if (totActiveTweens < 0)
            {
                Debugger.LogAddActiveTweenError("totActiveTweens < 0");
                totActiveTweens = 0;
            }
//            else if (totActiveTweens > _activeTweens.Length - 1) {
//                Debugger.LogError("AddActiveTween: totActiveTweens > _activeTweens capacity. This should never ever happen. Please report it with instructions on how to reproduce it");
//                return;
//            }

            t.Active = true;
            t.updateType = DoTween.defaultUpdateType;
            t.isIndependentUpdate = DoTween.defaultTimeScaleIndependent;
            t.activeId = maxActiveLookupId = totActiveTweens;
            activeTweens[totActiveTweens] = t;
            if (t.updateType == UpdateType.Normal)
            {
                totActiveDefaultTweens++;
                hasActiveDefaultTweens = true;
            }
            else
            {
                switch (t.updateType)
                {
                    case UpdateType.Fixed:
                        totActiveFixedTweens++;
                        hasActiveFixedTweens = true;
                        break;
                    case UpdateType.Late:
                        totActiveLateTweens++;
                        hasActiveLateTweens = true;
                        break;
                    default:
                        totActiveManualTweens++;
                        hasActiveManualTweens = true;
                        break;
                }
            }

            totActiveTweens++;
            if (t.tweenType == TweenType.Tweener) totActiveTweeners++;
            else totActiveSequences++;
            hasActiveTweens = true;
        }

        private static void ReorganizeActiveTweens()
        {
            if (totActiveTweens <= 0)
            {
                maxActiveLookupId = -1;
                requiresActiveReorganization = false;
                reorganizeFromId = -1;
                return;
            }
            else if (reorganizeFromId == maxActiveLookupId)
            {
                maxActiveLookupId--;
                requiresActiveReorganization = false;
                reorganizeFromId = -1;
                return;
            }

            var shift = 1;
            var len = maxActiveLookupId + 1;
            maxActiveLookupId = reorganizeFromId - 1;
            for (var i = reorganizeFromId + 1; i < len; ++i)
            {
                var t = activeTweens[i];
                if (t == null)
                {
                    shift++;
                    continue;
                }

                t.activeId = maxActiveLookupId = i - shift;
                activeTweens[i - shift] = t;
                activeTweens[i] = null;
            }

            requiresActiveReorganization = false;
            reorganizeFromId = -1;
        }

        private static void DespawnActiveTweens(List<Tween> tweens)
        {
            var count = tweens.Count - 1;
            for (var i = count; i > -1; --i) Despawn(tweens[i]);
        }

        // Removes a tween from the active list, then reorganizes said list and decreases the given total.
        // Also removes any TweenLinks associated to this tween.
        private static void RemoveActiveTween(Tween t)
        {
            var index = t.activeId;

            if (totTweenLinks > 0) RemoveTweenLink(t);

            t.activeId = -1;
            requiresActiveReorganization = true;
            if (reorganizeFromId == -1 || reorganizeFromId > index) reorganizeFromId = index;
            activeTweens[index] = null;

            if (t.updateType == UpdateType.Normal)
            {
                // Safety check (IndexOutOfRangeException)
                if (totActiveDefaultTweens > 0)
                {
                    totActiveDefaultTweens--;
                    hasActiveDefaultTweens = totActiveDefaultTweens > 0;
                }
                else
                {
                    Debugger.LogRemoveActiveTweenError("totActiveDefaultTweens < 0");
                }
            }
            else
            {
                switch (t.updateType)
                {
                    case UpdateType.Fixed:
                        // Safety check (IndexOutOfRangeException)
                        if (totActiveFixedTweens > 0)
                        {
                            totActiveFixedTweens--;
                            hasActiveFixedTweens = totActiveFixedTweens > 0;
                        }
                        else
                        {
                            Debugger.LogRemoveActiveTweenError("totActiveFixedTweens < 0");
                        }

                        break;
                    case UpdateType.Late:
                        // Safety check (IndexOutOfRangeException)
                        if (totActiveLateTweens > 0)
                        {
                            totActiveLateTweens--;
                            hasActiveLateTweens = totActiveLateTweens > 0;
                        }
                        else
                        {
                            Debugger.LogRemoveActiveTweenError("totActiveLateTweens < 0");
                        }

                        break;
                    default:
                        // Safety check (IndexOutOfRangeException)
                        if (totActiveManualTweens > 0)
                        {
                            totActiveManualTweens--;
                            hasActiveManualTweens = totActiveManualTweens > 0;
                        }
                        else
                        {
                            Debugger.LogRemoveActiveTweenError("totActiveManualTweens < 0");
                        }

                        break;
                }
            }

            totActiveTweens--;
            hasActiveTweens = totActiveTweens > 0;
            if (t.tweenType == TweenType.Tweener) totActiveTweeners--;
            else totActiveSequences--;
            // Safety check (IndexOutOfRangeException)
            if (totActiveTweens < 0)
            {
                totActiveTweens = 0;
                Debugger.LogRemoveActiveTweenError("totActiveTweens < 0");
            }

            // Safety check (IndexOutOfRangeException)
            if (totActiveTweeners < 0)
            {
                totActiveTweeners = 0;
                Debugger.LogRemoveActiveTweenError("totActiveTweeners < 0");
            }

            // Safety check (IndexOutOfRangeException)
            if (totActiveSequences < 0)
            {
                totActiveSequences = 0;
                Debugger.LogRemoveActiveTweenError("totActiveSequences < 0");
            }
        }

        private static void ClearTweenArray(Tween[] tweens)
        {
            var len = tweens.Length;
            for (var i = 0; i < len; i++) tweens[i] = null;
        }

        private static void IncreaseCapacities(CapacityIncreaseMode increaseMode)
        {
            var killAdd = 0;
//            int increaseTweenersBy = _DefaultMaxTweeners;
//            int increaseSequencesBy = _DefaultMaxSequences;
            var increaseTweenersBy = Mathf.Max((int) (maxTweeners * 1.5f), DEFAULT_MAX_TWEENERS);
            var increaseSequencesBy = Mathf.Max((int) (maxSequences * 1.5f), DEFAULT_MAX_SEQUENCES);
            switch (increaseMode)
            {
                case CapacityIncreaseMode.TweenersOnly:
                    killAdd += increaseTweenersBy;
                    maxTweeners += increaseTweenersBy;
                    Array.Resize(ref pooledTweeners, maxTweeners);
                    break;
                case CapacityIncreaseMode.SequencesOnly:
                    killAdd += increaseSequencesBy;
                    maxSequences += increaseSequencesBy;
                    break;
                default:
                    killAdd += increaseTweenersBy + increaseSequencesBy;
                    maxTweeners += increaseTweenersBy;
                    maxSequences += increaseSequencesBy;
                    Array.Resize(ref pooledTweeners, maxTweeners);
                    break;
            }

//            maxActive = Mathf.Max(maxTweeners, maxSequences);
            maxActive = maxTweeners + maxSequences;
            Array.Resize(ref activeTweens, maxActive);
            if (killAdd > 0) KillList.Capacity += killAdd;
        }

        #region Helpers

        // If isPlayBackwardsOrSmoothRewind is FALSE, it means this was a Rewind command
        private static void ManageOnRewindCallbackWhenAlreadyRewinded(Tween t, bool isPlayBackwardsOrSmoothRewind)
        {
            if (t.onRewind == null) return;
            if (isPlayBackwardsOrSmoothRewind)
            {
                // PlayBackwards or SmoothRewind
                if (DoTween.rewindCallbackMode == RewindCallbackMode.FireAlways) t.onRewind();
            }
            else
            {
                // Rewind
                if (DoTween.rewindCallbackMode != RewindCallbackMode.FireIfPositionChanged) t.onRewind();
            }
        }

        #endregion

        #endregion

        #region Debug Methods

#if DEBUG
        private static void VerifyActiveTweensList()
        {
            int nullTweensWithinLookup = 0, inactiveTweensWithinLookup = 0, activeTweensAfterNull = 0;
            var activeTweensAfterNullIds = new List<int>();

            for (var i = 0; i < maxActiveLookupId + 1; ++i)
            {
                var t = activeTweens[i];
                if (t == null) nullTweensWithinLookup++;
                else if (!t.Active) inactiveTweensWithinLookup++;
            }

            var len = activeTweens.Length;
            var firstNullIndex = -1;
            for (var i = 0; i < len; ++i)
            {
                if (firstNullIndex == -1 && activeTweens[i] == null) firstNullIndex = i;
                else if (firstNullIndex > -1 && activeTweens[i] != null)
                {
                    activeTweensAfterNull++;
                    activeTweensAfterNullIds.Add(i);
                }
            }

            if (nullTweensWithinLookup > 0 || inactiveTweensWithinLookup > 0 || activeTweensAfterNull > 0)
            {
                var s = "VerifyActiveTweensList WARNING:";
                if (isUpdateLoop) s += " - UPDATE LOOP (" + updateLoopCount + ")";
                if (nullTweensWithinLookup > 0) s += " - NULL Tweens Within Lookup (" + nullTweensWithinLookup + ")";
                if (inactiveTweensWithinLookup > 0) s += " - Inactive Tweens Within Lookup (" + inactiveTweensWithinLookup + ")";
                if (activeTweensAfterNull > 0)
                {
                    var indexes = "";
                    len = activeTweensAfterNullIds.Count;
                    for (var i = 0; i < len; ++i)
                    {
                        if (i > 0) indexes += ",";
                        indexes += activeTweensAfterNullIds[i];
                    }

                    s += " - Active tweens after NULL ones (" + (firstNullIndex - 1) + "/" + activeTweensAfterNull + "[" + indexes + "]" + ")";
                }

                Debug.LogWarning(s);
            }
        }
#endif

        #endregion

        #region public Classes

        // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        // ||| public CLASSES ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

        public enum CapacityIncreaseMode
        {
            TweenersAndSequences,
            TweenersOnly,
            SequencesOnly
        }

        #endregion
    }
}