﻿// Author: Daniele Giardini - http://www.demigiant.com
// Created: 2014/08/17 19:40
// 
// License Copyright (c) Daniele Giardini.
// This work is subject to the terms at http://dotween.demigiant.com/license.php

using System;
using System.Reflection;
using UnityEngine;

namespace DG.Tweening.Core
{
    public static class Utils
    {
        private static Assembly[] loadedAssemblies;

        private static readonly string[] DefAssembliesToQuery = new[] { // First assemblies to look into before checking all of them
            "DOTween.Modules", "Assembly-CSharp", "Assembly-CSharp-firstpass"
        };

        /// <summary>
        /// Returns a Vector3 with z = 0
        /// </summary>
        public static Vector3 Vector3FromAngle(float degrees, float magnitude)
        {
            var radians = degrees * Mathf.Deg2Rad;
            return new Vector3(magnitude * Mathf.Cos(radians), magnitude * Mathf.Sin(radians), 0);
        }

        /// <summary>
        /// Returns the 2D angle between two vectors
        /// </summary>
        public static float Angle2D(Vector3 from, Vector3 to)
        {
            var baseDir = Vector2.right;
            to -= from;
            var ang = Vector2.Angle(baseDir, to);
            var cross = Vector3.Cross(baseDir, to);
            if (cross.z > 0) ang = 360 - ang;
            ang *= -1f;
            return ang;
        }

        public static Vector3 RotateAroundPivot(Vector3 point, Vector3 pivot, Quaternion rotation)
        {
            return rotation * (point - pivot) + pivot;
        }

        /// <summary>
        /// Uses approximate equality on each axis instead of Unity's Vector3 equality,
        /// because the latter fails (in some cases) when assigning a Vector3 to a transform.position and then checking it.
        /// </summary>
        public static bool Vector3AreApproximatelyEqual(Vector3 a, Vector3 b)
        {
            return Mathf.Approximately(a.x, b.x)
                   && Mathf.Approximately(a.y, b.y)
                   && Mathf.Approximately(a.z, b.z);
        }

        /// <summary>
        /// Looks for the type within all possible project assembly names
        /// </summary>
        public static Type GetLooseScriptType(string typeName)
        {
            // Check in default assemblies (Unity 2017 and later)
            for (var i = 0; i < DefAssembliesToQuery.Length; ++i) {
                var result = Type.GetType($"{typeName}, {DefAssembliesToQuery[i]}");
                if (result == null) continue;
                return result;
            }
            // Check in all assemblies
            if (loadedAssemblies == null) loadedAssemblies = AppDomain.CurrentDomain.GetAssemblies();
            for (var i = 0; i < loadedAssemblies.Length; ++i) {
                var result = Type.GetType($"{typeName}, {loadedAssemblies[i].GetName()}");
                if (result == null) continue;
                return result;
            }
            return null;
        }

        // █████████████████████████████████████████████████████████████████████████████████████████████████████████████████████
        // ███ public CLASSES ████████████████████████████████████████████████████████████████████████████████████████████████
        // █████████████████████████████████████████████████████████████████████████████████████████████████████████████████████

        // Uses code from BK > http://stackoverflow.com/a/1280832
        // (scrapped > doesn't work with IL2CPP)
//        public class InstanceCreator<T> where T : new()
//        {
//            static readonly Expression<Func<T>> _TExpression = () => new T();
//            static readonly Func<T> _TBuilder = _TExpression.Compile();
//            public static T Create() { return _TBuilder(); }
//        }
    }
}