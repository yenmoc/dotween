﻿// Author: Daniele Giardini - http://www.demigiant.com
// Created: 2014/09/04 11:01
// 
// License Copyright (c) Daniele Giardini.
// This work is subject to the terms at http://dotween.demigiant.com/license.php

using System;
using UnityEngine;

// ReSharper disable once CheckNamespace
namespace DG.Tweening.Plugins.Core.PathCore
{
    public class CatmullRomDecoder : AbsPathDecoder
    {
        // Used for temporary operations
        private static readonly ControlPoint[] PartialControlPs = new ControlPoint[2];
        private static readonly Vector3[] PartialWps = new Vector3[2];

        #region Methods

        public override void FinalizePath(Path p, Vector3[] wps, bool isClosedPath)
        {
            // Add starting and ending control points (uses only one vector per control point)
            var wpsLen = wps.Length;
            if (p.controlPoints == null || p.controlPoints.Length != 2) p.controlPoints = new ControlPoint[2];
            if (isClosedPath) {
                p.controlPoints[0] = new ControlPoint(wps[wpsLen - 2], Vector3.zero);
                p.controlPoints[1] = new ControlPoint(wps[1], Vector3.zero);
            } else {
                p.controlPoints[0] = new ControlPoint(wps[1], Vector3.zero);
                var lastP = wps[wpsLen - 1];
                var diffV = lastP - wps[wpsLen - 2];
                p.controlPoints[1] = new ControlPoint(lastP + diffV, Vector3.zero);
            }
            // Store total subdivisions
//            p.subdivisions = (wpsLen + 2) * p.subdivisionsXSegment;
            p.subdivisions = wpsLen * p.subdivisionsXSegment;
            // Store time to len tables
            SetTimeToLengthTables(p, p.subdivisions);
            // Store waypoints lengths
            SetWaypointsLengths(p, p.subdivisionsXSegment);
        }

        // controlPoints as a separate parameter so we can pass custom ones from SetWaypointsLengths
        public override Vector3 GetPoint(float perc, Vector3[] wps, Path p, ControlPoint[] controlPoints)
        {
            var numSections = wps.Length - 1; // Considering also control points
            var tSec = (int)Math.Floor(perc * numSections);
            var currPt = numSections - 1;
            if (currPt > tSec) currPt = tSec;
            var u = perc * numSections - currPt;

            var a = currPt == 0 ? controlPoints[0].a : wps[currPt - 1];
            var b = wps[currPt];
            var c = wps[currPt + 1];
            var d = currPt + 2 > wps.Length - 1 ? controlPoints[1].a : wps[currPt + 2];

            return .5f * (
                (-a + 3f * b - 3f * c + d) * (u * u * u)
                + (2f * a - 5f * b + 4f * c - d) * (u * u)
                + (-a + c) * u
                + 2f * b
            );
        }

        public void SetTimeToLengthTables(Path p, int subdivisions)
        {
            float pathLen = 0;
            var incr = 1f / subdivisions;
            var timesTable = new float[subdivisions];
            var lengthsTable = new float[subdivisions];
            var prevP = GetPoint(0, p.wps, p, p.controlPoints);
            for (var i = 1; i < subdivisions + 1; ++i) {
                var perc = incr * i;
                var currP = GetPoint(perc, p.wps, p, p.controlPoints);
                pathLen += Vector3.Distance(currP, prevP);
                prevP = currP;
                timesTable[i - 1] = perc;
                lengthsTable[i - 1] = pathLen;
            }

            // Assign
            p.length = pathLen;
            p.timesTable = timesTable;
            p.lengthsTable = lengthsTable;
        }

        public void SetWaypointsLengths(Path p, int subdivisions)
        {
            // Create a relative path between each waypoint,
            // with its start and end control lines coinciding with the next/prev waypoints.
            var count = p.wps.Length;
            var wpLengths = new float[count];
            wpLengths[0] = 0;
            for (var i = 1; i < count; ++i) {
                // Create partial path
                PartialControlPs[0].a = i == 1 ? p.controlPoints[0].a : p.wps[i - 2];
                PartialWps[0] = p.wps[i - 1];
                PartialWps[1] = p.wps[i];
                PartialControlPs[1].a = i == count - 1 ? p.controlPoints[1].a : p.wps[i + 1];
                // Calculate length of partial path
                float partialLen = 0;
                var incr = 1f / subdivisions;
                var prevP = GetPoint(0, PartialWps, p, PartialControlPs);
                for (var c = 1; c < subdivisions + 1; ++c) {
                    var perc = incr * c;
                    var currP = GetPoint(perc, PartialWps, p, PartialControlPs);
                    partialLen += Vector3.Distance(currP, prevP);
                    prevP = currP;
                }
                wpLengths[i] = partialLen;
            }

            // Assign
            p.wpLengths = wpLengths;
        }

        #endregion
    }
}