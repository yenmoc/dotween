﻿// Author: Daniele Giardini - http://www.demigiant.com
// Created: 2014/09/04 11:02
// 
// License Copyright (c) Daniele Giardini.
// This work is subject to the terms at http://dotween.demigiant.com/license.php

using UnityEngine;

namespace DG.Tweening.Plugins.Core.PathCore
{
    public class LinearDecoder : AbsPathDecoder
    {
        public override void FinalizePath(Path p, Vector3[] wps, bool isClosedPath)
        {
            p.controlPoints = null;
            // Store time to len tables
            p.subdivisions = (wps.Length) * p.subdivisionsXSegment; // Unused
            SetTimeToLengthTables(p, p.subdivisions);
        }

        public override Vector3 GetPoint(float perc, Vector3[] wps, Path p, ControlPoint[] controlPoints)
        {
            if (perc <= 0) {
                p.linearWpIndex = 1;
                return wps[0];
            }

            var startPIndex = 0;
            var endPIndex = 0;
            var count = p.timesTable.Length;
            for (var i = 1; i < count; i++) {
                if (p.timesTable[i] >= perc) {
                    startPIndex = i - 1;
                    endPIndex = i;
                    break;
                }
            }

            var startPPerc = p.timesTable[startPIndex];
            var partialPerc = perc - startPPerc;
            var partialLen = p.length * partialPerc;
            var wp0 = wps[startPIndex];
            var wp1 = wps[endPIndex];
            p.linearWpIndex = endPIndex;
            return wp0 + Vector3.ClampMagnitude(wp1 - wp0, partialLen);
        }

        // Linear exception: also sets waypoints lengths and doesn't set lengthsTable since it's useless
        public void SetTimeToLengthTables(Path p, int subdivisions)
        {
            float pathLen = 0;
            var wpsLen = p.wps.Length;
            var wpLengths = new float[wpsLen];
            var prevP = p.wps[0];
            for (var i = 0; i < wpsLen; i++) {
                var currP = p.wps[i];
                var dist = Vector3.Distance(currP, prevP);
                pathLen += dist;
                prevP = currP;
                wpLengths[i] = dist;
            }
            var timesTable = new float[wpsLen];
            float tmpLen = 0;
            for (var i = 1; i < wpsLen; i++) {
                tmpLen += wpLengths[i];
                timesTable[i] = tmpLen / pathLen;
            }

            // Assign
            p.length = pathLen;
            p.wpLengths = wpLengths;
            p.timesTable = timesTable;
        }

        public void SetWaypointsLengths(Path p, int subdivisions)
        {
            // Does nothing (waypoints lenghts were stored inside SetTimeToLengthTables)
        }
    }
}