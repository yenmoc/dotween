﻿// Author: Daniele Giardini - http://www.demigiant.com
// Created: 2014/05/06 18:11
// 
// License Copyright (c) Daniele Giardini.
// This work is subject to the terms at http://dotween.demigiant.com/license.php

#if COMPATIBLE
using DOVector2 = DG.Tweening.Core.Surrogates.Vector2Wrapper;
using DOVector3 = DG.Tweening.Core.Surrogates.Vector3Wrapper;
using DOVector4 = DG.Tweening.Core.Surrogates.Vector4Wrapper;
using DOQuaternion = DG.Tweening.Core.Surrogates.QuaternionWrapper;
using DOColor = DG.Tweening.Core.Surrogates.ColorWrapper;
using DOVector2Plugin = DG.Tweening.Plugins.Vector2WrapperPlugin;
using DOVector3Plugin = DG.Tweening.Plugins.Vector3WrapperPlugin;
using DOVector4Plugin = DG.Tweening.Plugins.Vector4WrapperPlugin;
using DOQuaternionPlugin = DG.Tweening.Plugins.QuaternionWrapperPlugin;
using DOColorPlugin = DG.Tweening.Plugins.ColorWrapperPlugin;
#else
using DOVector2 = UnityEngine.Vector2;
using DOVector3 = UnityEngine.Vector3;
using DOVector4 = UnityEngine.Vector4;
using DOQuaternion = UnityEngine.Quaternion;
using DOColor = UnityEngine.Color;
using DOVector2Plugin = DG.Tweening.Plugins.Vector2Plugin;
using DOVector3Plugin = DG.Tweening.Plugins.Vector3Plugin;
using DOVector4Plugin = DG.Tweening.Plugins.Vector4Plugin;
using DOQuaternionPlugin = DG.Tweening.Plugins.QuaternionPlugin;
using DOColorPlugin = DG.Tweening.Plugins.ColorPlugin;
#endif
using System;
using System.Collections.Generic;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using UnityEngine;

namespace DG.Tweening.Plugins.Core
{
    public static class PluginsManager
    {
        // Default plugins
        private static ITweenPlugin floatPlugin;
        private static ITweenPlugin doublePlugin;
        private static ITweenPlugin intPlugin;
        private static ITweenPlugin uintPlugin;
        private static ITweenPlugin longPlugin;
        private static ITweenPlugin ulongPlugin;
        private static ITweenPlugin vector2Plugin;
        private static ITweenPlugin vector3Plugin;
        private static ITweenPlugin vector4Plugin;
        private static ITweenPlugin quaternionPlugin;
        private static ITweenPlugin colorPlugin;
        private static ITweenPlugin rectPlugin;
        private static ITweenPlugin rectOffsetPlugin;
        private static ITweenPlugin stringPlugin;
        private static ITweenPlugin vector3ArrayPlugin;
        private static ITweenPlugin color2Plugin;

        // Advanced and custom plugins
        private const int MAX_CUSTOM_PLUGINS = 20;
        private static Dictionary<Type, ITweenPlugin> customPlugins;

        // ===================================================================================
        // public METHODS ------------------------------------------------------------------

        public static AbsTweenPlugin<T1,T2,TPlugOptions> GetDefaultPlugin<T1,T2,TPlugOptions>() where TPlugOptions : struct, IPlugOptions
        {
            var t1 = typeof(T1);
            var t2 = typeof(T2);
            ITweenPlugin plugin = null;
            if (t1 == typeof(DOVector3) && t1 == t2) {
                if (vector3Plugin == null) vector3Plugin = new DOVector3Plugin();
                plugin = vector3Plugin;
            } else if (t1 == typeof(Vector3) && t2 == typeof(Vector3[])) {
                if (vector3ArrayPlugin == null) vector3ArrayPlugin = new Vector3ArrayPlugin();
                plugin = vector3ArrayPlugin;
            } else if (t1 == typeof(DOQuaternion)) {
                if (t2 == typeof(Quaternion)) Debugger.LogError("Quaternion tweens require a Vector3 endValue");
                else {
                    if (quaternionPlugin == null) quaternionPlugin = new DOQuaternionPlugin();
                    plugin = quaternionPlugin;
                }
            } else if (t1 == typeof(DOVector2)) {
                if (vector2Plugin == null) vector2Plugin = new DOVector2Plugin();
                plugin = vector2Plugin;
            } else if (t1 == typeof(float)) {
                if (floatPlugin == null) floatPlugin = new FloatPlugin();
                plugin = floatPlugin;
            } else if (t1 == typeof(DOColor)) {
                if (colorPlugin == null) colorPlugin = new DOColorPlugin();
                plugin = colorPlugin;
            } else if (t1 == typeof(int)) {
                if (intPlugin == null) intPlugin = new IntPlugin();
                plugin = intPlugin;
            } else if (t1 == typeof(DOVector4)) {
                if (vector4Plugin == null) vector4Plugin = new DOVector4Plugin();
                plugin = vector4Plugin;
            } else if (t1 == typeof(Rect)) {
                if (rectPlugin == null) rectPlugin = new RectPlugin();
                plugin = rectPlugin;
            } else if (t1 == typeof(RectOffset)) {
                if (rectOffsetPlugin == null) rectOffsetPlugin = new RectOffsetPlugin();
                plugin = rectOffsetPlugin;
            } else if (t1 == typeof(uint)) {
                if (uintPlugin == null) uintPlugin = new UintPlugin();
                plugin = uintPlugin;
            } else if (t1 == typeof(string)) {
                if (stringPlugin == null) stringPlugin = new StringPlugin();
                plugin = stringPlugin;
            } else if (t1 == typeof(Color2)) {
                if (color2Plugin == null) color2Plugin = new Color2Plugin();
                plugin = color2Plugin;
            } else if (t1 == typeof(long)) {
                if (longPlugin == null) longPlugin = new LongPlugin();
                plugin = longPlugin;
            } else if (t1 == typeof(ulong)) {
                if (ulongPlugin == null) ulongPlugin = new UlongPlugin();
                plugin = ulongPlugin;
            } else if (t1 == typeof(double)) {
                if (doublePlugin == null) doublePlugin = new DoublePlugin();
                plugin = doublePlugin;
            }

            if (plugin != null) return plugin as AbsTweenPlugin<T1, T2, TPlugOptions>;

            return null;
        }

        // Public so it can be used by custom plugins Get method
        public static AbsTweenPlugin<T1, T2, TPlugOptions> GetCustomPlugin<TPlugin, T1, T2, TPlugOptions>()
            where TPlugin : ITweenPlugin, new()
            where TPlugOptions : struct, IPlugOptions
        {
            var t = typeof(TPlugin);
            ITweenPlugin plugin;

            if (customPlugins == null) customPlugins = new Dictionary<Type, ITweenPlugin>(MAX_CUSTOM_PLUGINS);
            else if (customPlugins.TryGetValue(t, out plugin)) return plugin as AbsTweenPlugin<T1, T2, TPlugOptions>;

            plugin = new TPlugin();
            customPlugins.Add(t, plugin);
            return plugin as AbsTweenPlugin<T1, T2, TPlugOptions>;
        }

        // Un-caches all plugins
        public static void PurgeAll()
        {
            floatPlugin = null;
            intPlugin = null;
            uintPlugin = null;
            longPlugin = null;
            ulongPlugin = null;
            vector2Plugin = null;
            vector3Plugin = null;
            vector4Plugin = null;
            quaternionPlugin = null;
            colorPlugin = null;
            rectPlugin = null;
            rectOffsetPlugin = null;
            stringPlugin = null;
            vector3ArrayPlugin = null;
            color2Plugin = null;

            if (customPlugins != null) customPlugins.Clear();
        }
    }
}